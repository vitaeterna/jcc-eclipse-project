package jcc.exercise16;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class P2PChatFX extends Application
{
    private P2PChatFX               _this                 = null;

    private Scene                   scene                 = null;
    private HashMap<String, Parent> sceneMap              = new HashMap<>();

    private PrintWriter             writer                = null;

    private Button                  start_buttonHost      = null;
    private Button                  start_buttonConnect   = null;

    private TextField               host_textFieldPort    = null;
    private Button                  host_buttonHost       = null;
    private Button                  host_buttonBack       = null;

    private TextField               connect_textFieldIP   = null;
    private TextField               connect_textFieldPort = null;
    private Button                  connect_buttonConnect = null;
    private Button                  connect_buttonBack    = null;

    private TextArea                chat_textAreaHistory  = null;
    private TextArea                chat_textAreaInput    = null;
    private Button                  chat_buttonSend       = null;
    private Button                  chat_buttonDisconnect = null;

    public static void main(String[] args)
    {
        launch(args);
    }

    public void setScene(String key)
    {
        scene.setRoot(sceneMap.get(key));
    }

    public void setWriter(PrintWriter writer)
    {
        if(writer == null && this.writer != null)
        {
            this.writer.close();
        }

        this.writer = writer;
    }

    public void appendHistory(String message)
    {
        chat_textAreaHistory.appendText(message);
    }

    public void disableOnHost()
    {
        host_textFieldPort.disableProperty().set(true);
        host_buttonHost.disableProperty().set(true);
        start_buttonConnect.disableProperty().set(true);
    }

    public void disableOnDisconnect()
    {
        chat_textAreaInput.disableProperty().set(true);
        chat_buttonSend.disableProperty().set(true);
        chat_buttonDisconnect.disableProperty().set(true);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        _this = this;

        createStartScene();
        createConnectionScene();
        createHostScene();
        createChatScene();

        scene = new Scene(sceneMap.get("start"), 500, 500);

        primaryStage.setTitle("P2PChatFX");
        primaryStage.setMinWidth(500);
        primaryStage.setMinHeight(500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void createStartScene()
    {
        start_buttonHost = new Button("Host");
        start_buttonHost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        start_buttonHost.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                setScene("host");
            }
        });

        start_buttonConnect = new Button("Connect");
        start_buttonConnect.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        start_buttonConnect.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                setScene("connect");
            }
        });

        ColumnConstraints columnConstraint = new ColumnConstraints();

        columnConstraint.setPercentWidth(50);
        columnConstraint.setHalignment(HPos.CENTER);
        columnConstraint.setHgrow(Priority.ALWAYS);

        RowConstraints rowConstraint = new RowConstraints();

        rowConstraint.setPercentHeight(100);
        rowConstraint.setValignment(VPos.CENTER);
        rowConstraint.setVgrow(Priority.ALWAYS);

        GridPane gridPaneStart = new GridPane();
        gridPaneStart.getColumnConstraints().addAll(columnConstraint, columnConstraint);
        gridPaneStart.getRowConstraints().addAll(rowConstraint);
        gridPaneStart.setHgap(5);
        gridPaneStart.setVgap(5);
        gridPaneStart.setPadding(new Insets(10, 10, 10, 10));
        gridPaneStart.add(start_buttonHost, 0, 0);
        gridPaneStart.add(start_buttonConnect, 1, 0);

        sceneMap.put("start", gridPaneStart);
    }

    private void createConnectionScene()
    {
        Label labelSocket = new Label("Connect to Remote Socket");
        labelSocket.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        Label labelIP = new Label("IP-Address:");

        connect_textFieldIP = new TextField();
        connect_textFieldIP.alignmentProperty().set(Pos.CENTER_LEFT);
        connect_textFieldIP.setText("");
        connect_textFieldIP.editableProperty().set(true);
        connect_textFieldIP.disableProperty().set(false);

        Label labelPort = new Label("Port:");

        connect_textFieldPort = new TextField();
        connect_textFieldPort.alignmentProperty().set(Pos.CENTER_LEFT);
        connect_textFieldPort.setText("");
        connect_textFieldPort.editableProperty().set(true);
        connect_textFieldPort.disableProperty().set(false);

        connect_buttonConnect = new Button("Connect");
        connect_buttonConnect.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        connect_buttonConnect.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                try
                {
                    TCPHandler.connect(connect_textFieldIP.getText(), Integer.parseInt(connect_textFieldPort.getText()), _this);
                }
                catch(NumberFormatException e)
                {
                    System.err.println("Invalid Port!");
                }
            }
        });

        connect_buttonBack = new Button("Back");
        connect_buttonBack.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        connect_buttonBack.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                setScene("start");
            }
        });

        ColumnConstraints columnConstraint = new ColumnConstraints();

        columnConstraint.setPercentWidth(25);
        columnConstraint.setHalignment(HPos.LEFT);
        columnConstraint.setHgrow(Priority.ALWAYS);

        RowConstraints rowConstraint = new RowConstraints();

        rowConstraint.setPercentHeight(25);
        rowConstraint.setValignment(VPos.CENTER);
        rowConstraint.setVgrow(Priority.ALWAYS);

        GridPane gridPaneConnect = new GridPane();
        gridPaneConnect.getColumnConstraints().addAll(columnConstraint, columnConstraint, columnConstraint, columnConstraint);
        gridPaneConnect.getRowConstraints().addAll(rowConstraint, rowConstraint, rowConstraint, rowConstraint);
        gridPaneConnect.setHgap(5);
        gridPaneConnect.setVgap(5);
        gridPaneConnect.setPadding(new Insets(10, 10, 10, 10));
        gridPaneConnect.add(labelSocket, 0, 0, 4, 1);
        gridPaneConnect.add(labelIP, 0, 1);
        gridPaneConnect.add(connect_textFieldIP, 1, 1, 3, 1);
        gridPaneConnect.add(labelPort, 0, 2);
        gridPaneConnect.add(connect_textFieldPort, 1, 2, 3, 1);
        gridPaneConnect.add(connect_buttonConnect, 0, 3, 2, 1);
        gridPaneConnect.add(connect_buttonBack, 2, 3, 2, 1);

        sceneMap.put("connect", gridPaneConnect);
    }

    private void createHostScene()
    {
        Label labelSocket = new Label("Create Local Socket");
        labelSocket.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        Label labelPort = new Label("Port:");

        host_textFieldPort = new TextField();
        host_textFieldPort.alignmentProperty().set(Pos.CENTER_LEFT);
        host_textFieldPort.setText("");
        host_textFieldPort.editableProperty().set(true);
        host_textFieldPort.disableProperty().set(false);

        host_buttonHost = new Button("Host");
        host_buttonHost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        host_buttonHost.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                try
                {
                    TCPHandler.host(Integer.parseInt(host_textFieldPort.getText()), _this);
                }
                catch(NumberFormatException e)
                {
                    System.err.println("Invalid Port!");
                }
            }
        });

        host_buttonBack = new Button("Back");
        host_buttonBack.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        host_buttonBack.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                setScene("start");
            }
        });

        ColumnConstraints columnConstraint = new ColumnConstraints();

        columnConstraint.setPercentWidth(25);
        columnConstraint.setHalignment(HPos.LEFT);
        columnConstraint.setHgrow(Priority.ALWAYS);

        RowConstraints rowConstraint = new RowConstraints();

        rowConstraint.setPercentHeight(100.0d / 3.0d);
        rowConstraint.setValignment(VPos.CENTER);
        rowConstraint.setVgrow(Priority.ALWAYS);

        GridPane gridPaneHost = new GridPane();
        gridPaneHost.getColumnConstraints().addAll(columnConstraint, columnConstraint, columnConstraint, columnConstraint);
        gridPaneHost.getRowConstraints().addAll(rowConstraint, rowConstraint, rowConstraint);
        gridPaneHost.setHgap(5);
        gridPaneHost.setVgap(5);
        gridPaneHost.setPadding(new Insets(10, 10, 10, 10));
        gridPaneHost.add(labelSocket, 0, 0, 4, 1);
        gridPaneHost.add(labelPort, 0, 1);
        gridPaneHost.add(host_textFieldPort, 1, 1, 3, 1);
        gridPaneHost.add(host_buttonHost, 0, 2, 2, 1);
        gridPaneHost.add(host_buttonBack, 2, 2, 2, 1);

        sceneMap.put("host", gridPaneHost);
    }

    private void createChatScene()
    {
        chat_textAreaHistory = new TextArea();
        chat_textAreaHistory.editableProperty().set(false);
        chat_textAreaHistory.disableProperty().set(false);

        chat_textAreaInput = new TextArea();

        chat_buttonSend = new Button("Send");
        chat_buttonSend.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        chat_buttonSend.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                if(writer != null)
                {
                    writer.println(chat_textAreaInput.getText());

                    synchronized(chat_textAreaHistory)
                    {
                        chat_textAreaHistory.appendText(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + " Local:\n" + chat_textAreaInput.getText() + "\n");
                    }

                    chat_textAreaInput.clear();
                }
            }
        });

        chat_buttonDisconnect = new Button("Disconnect");
        chat_buttonDisconnect.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        chat_buttonDisconnect.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                writer.println("/disconnect");

                TCPHandler.close();
            }
        });

        ColumnConstraints columnConstraint = new ColumnConstraints();

        columnConstraint.setPercentWidth(50);
        columnConstraint.setHalignment(HPos.CENTER);
        columnConstraint.setHgrow(Priority.ALWAYS);

        RowConstraints rowConstraint0 = new RowConstraints();

        rowConstraint0.setPercentHeight(40);
        rowConstraint0.setValignment(VPos.CENTER);
        rowConstraint0.setVgrow(Priority.ALWAYS);

        RowConstraints rowConstraint1 = new RowConstraints();

        rowConstraint1.setPercentHeight(20);
        rowConstraint1.setValignment(VPos.CENTER);
        rowConstraint1.setVgrow(Priority.ALWAYS);

        GridPane gridPaneChat = new GridPane();
        gridPaneChat.getColumnConstraints().addAll(columnConstraint, columnConstraint);
        gridPaneChat.getRowConstraints().addAll(rowConstraint0, rowConstraint0, rowConstraint1);
        gridPaneChat.setHgap(5);
        gridPaneChat.setVgap(5);
        gridPaneChat.setPadding(new Insets(10, 10, 10, 10));
        gridPaneChat.add(chat_textAreaHistory, 0, 0, 2, 1);
        gridPaneChat.add(chat_textAreaInput, 0, 1, 2, 1);
        gridPaneChat.add(chat_buttonSend, 0, 2);
        gridPaneChat.add(chat_buttonDisconnect, 1, 2);

        sceneMap.put("chat", gridPaneChat);
    }
}
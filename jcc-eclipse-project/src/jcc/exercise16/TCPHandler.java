package jcc.exercise16;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TCPHandler
{
    private static Thread         serverThread = null;

    private static Socket         clientSocket = null;

    private static BufferedReader reader       = null;
    private static PrintWriter    writer       = null;

    public static void host(int port, P2PChatFX application)
    {
        if(port < 0 || port > 65535)
        {
            System.err.println("Port out of range [0;65535]");

            return;
        }

        serverThread = new Thread()
        {
            @SuppressWarnings("resource")
            @Override
            public void run()
            {
                ServerSocket serverSocket = null;

                try
                {
                    serverSocket = new ServerSocket(port);
                }
                catch(IOException e)
                {
                    System.err.println("Port " + port + " already in use.");

                    return;
                }

                application.disableOnHost();

                System.out.println("Listening on port " + port);

                try
                {
                    clientSocket = serverSocket.accept();

                    writer = new PrintWriter(clientSocket.getOutputStream(), true);

                    application.setWriter(writer);

                    application.setScene("chat");

                    reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                    String line = null;

                    while((line = reader.readLine()) != null)
                    {
                        if(line.equals("/disconnect"))
                        {
                            throw new IOException();
                        }
                        else
                        {
                            application.appendHistory(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + " Remote:\n" + line + "\n");
                        }
                    }
                }
                catch(IOException e)
                {
                    application.setWriter(null);

                    application.disableOnDisconnect();

                    application.appendHistory("### Disconnected ###");
                }
                finally
                {
                    try
                    {
                        reader.close();
                    }
                    catch(IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        };

        serverThread.start();

    }

    public static void connect(String IPaddress, int port, P2PChatFX application)
    {
        if(port < 0 || port > 65535)
        {
            System.err.println("Port out of range [0;65535]");

            return;
        }

        try
        {
            clientSocket = new Socket(IPaddress, port);
        }
        catch(IOException e)
        {
            System.err.println("Connection not possible!");

            return;
        }

        try
        {
            writer = new PrintWriter(clientSocket.getOutputStream(), true);

            application.setWriter(writer);

            serverThread = new Thread()
            {
                @Override
                public void run()
                {
                    try
                    {
                        reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                        String line = null;

                        while((line = reader.readLine()) != null)
                        {
                            if(line.equals("/disconnect"))
                            {
                                throw new IOException();
                            }
                            else
                            {
                                application.appendHistory(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + " Remote:\n" + line + "\n");
                            }
                        }
                    }
                    catch(IOException e)
                    {
                        application.setWriter(null);

                        application.disableOnDisconnect();

                        application.appendHistory("### Disconnected ###");
                    }
                    finally
                    {
                        try
                        {
                            reader.close();
                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            };

            serverThread.start();

            application.setScene("chat");
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void close()
    {
        if(clientSocket != null && !clientSocket.isClosed())
        {
            try
            {
                clientSocket.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
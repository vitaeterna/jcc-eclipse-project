package jcc.exercise19;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

public class LineSegmentTest
{
    @Test
    public void getDistanceTest1()
    {
        LineSegment lineSegment = new LineSegment(3, 3, 3, 3);

        assertEquals(lineSegment.getDistance(), 0);
    }

    @Test
    public void getDistanceTest2()
    {
        LineSegment lineSegment = new LineSegment(-2, 0, 3, 0);

        assertEquals(lineSegment.getDistance(), 5);
    }

    @Test
    public void getDistanceTest3()
    {
        LineSegment lineSegment = new LineSegment(0, 3, 0, -2);

        assertEquals(lineSegment.getDistance(), 5);
    }

    @Test
    public void getDistanceTest4()
    {
        LineSegment lineSegment = new LineSegment(-1, -1, 1, 1);

        assertEquals(lineSegment.getDistance(), 2 * Math.sqrt(2));
    }

    @Test
    public void getDistanceTest5()
    {
        LineSegment lineSegment = new LineSegment(-1, 1, 1, -1);

        assertEquals(lineSegment.getDistance(), 2 * Math.sqrt(2));
    }

    @Test
    public void getSlopeTest1()
    {
        LineSegment lineSegment = new LineSegment(1, 1, 2, 2);

        assertEquals(lineSegment.getSlope(), 1);
    }

    @Test
    public void getSlopeTest2()
    {
        LineSegment lineSegment = new LineSegment(-2, 2, -1, 1);

        assertEquals(lineSegment.getSlope(), -1);
    }

    @Test
    public void getSlopeTest3()
    {
        LineSegment lineSegment = new LineSegment(-1, -1, 0, 1);

        assertEquals(lineSegment.getSlope(), 2);
    }

    @Test
    public void getSlopeTest4()
    {
        LineSegment lineSegment = new LineSegment(0, 0, 1, 0);

        assertEquals(lineSegment.getSlope(), 0);
    }

    @Test
    public void getSlopeTest5()
    {
        LineSegment lineSegment = new LineSegment(0, 0, 0, 1);

        assertThrows(ArithmeticException.class, () -> lineSegment.getSlope());
    }

    @Test
    public void parallelToTest1()
    {
        LineSegment lineSegment0 = new LineSegment(1, 1, 2, 2);
        LineSegment lineSegment1 = mock(LineSegment.class);

        when(lineSegment1.getSlope()).thenReturn(1.0d);

        assertTrue(lineSegment0.parallelTo(lineSegment1));

        verify(lineSegment1).getSlope();
    }

    @Test
    public void parallelToTest2()
    {
        LineSegment lineSegment0 = new LineSegment(2, 2, 1, 1);
        LineSegment lineSegment1 = mock(LineSegment.class);

        when(lineSegment1.getSlope()).thenReturn(1.0d);

        assertTrue(lineSegment0.parallelTo(lineSegment1));

        verify(lineSegment1).getSlope();
    }

    @Test
    public void parallelToTest3()
    {
        LineSegment lineSegment0 = new LineSegment(-1, 1, -2.001, 2);
        LineSegment lineSegment1 = mock(LineSegment.class);

        when(lineSegment1.getSlope()).thenReturn(-1.0d);

        assertFalse(lineSegment0.parallelTo(lineSegment1));

        verify(lineSegment1).getSlope();
    }

    @Test
    public void parallelToTest4()
    {
        LineSegment lineSegment0 = new LineSegment(-1, 0, 1, 0);
        LineSegment lineSegment1 = mock(LineSegment.class);

        when(lineSegment1.getSlope()).thenReturn(0.0d);

        assertTrue(lineSegment0.parallelTo(lineSegment1));

        verify(lineSegment1).getSlope();
    }

    @Test
    public void parallelToTest5()
    {
        LineSegment lineSegment0 = new LineSegment(0, 0, 0, 1);
        LineSegment lineSegment1 = mock(LineSegment.class);

        when(lineSegment1.getSlope()).thenReturn(1.0d);

        assertThrows(ArithmeticException.class, () -> lineSegment0.parallelTo(lineSegment1));
    }
}
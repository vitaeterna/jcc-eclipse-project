package jcc.exercise19;

public class LineSegment
{
    private double p0x = 0.0d;
    private double p0y = 0.0d;
    private double p1x = 0.0d;
    private double p1y = 0.0d;

    public LineSegment(double p0x, double p0y, double p1x, double p1y)
    {
        this.p0x = p0x;
        this.p0y = p0y;
        this.p1x = p1x;
        this.p1y = p1y;
    }

    public double getDistance()
    {
        return Math.sqrt(Math.pow(p1x - p0x, 2.0d) + Math.pow(p1y - p0y, 2.0d));
    }

    public double getSlope()
    {
        if(p0x == p1x)
        {
            throw new ArithmeticException();
        }

        return (p1y - p0y) / (p1x - p0x);
    }

    public boolean parallelTo(LineSegment lineSegment)
    {
        return Math.abs(getSlope() - lineSegment.getSlope()) < .0001;
    }
}
package jcc.exercise18;

public class NQueensApplication
{
    public static void main(String[] args)
    {
        NQueensSolver nQueensSolver = new NQueensSolver();

        for(int n = 0; n <= 10; n++)
        {
            try
            {
                nQueensSolver.solve(n);
                nQueensSolver.printBoard();
            }
            catch(IllegalArgumentException e)
            {
                System.out.println("Not defined for n=" + n + "\n");
            }
            catch(NQueensNotSolvableException e)
            {
                System.out.println("Not solvable for n=" + n + "\n");
            }
        }
    }
}
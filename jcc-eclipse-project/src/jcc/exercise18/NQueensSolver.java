package jcc.exercise18;

public class NQueensSolver
{
    private int         n     = 0;

    private boolean[][] board = null;

    public boolean[][] solve(int n) throws NQueensNotSolvableException, IllegalArgumentException
    {
        if(n < 1)
        {
            throw new IllegalArgumentException();
        }

        this.n = n;
        board = new boolean[n][n];

        for(int y = 0; y < n; y++)
        {
            for(int x = 0; x < n; x++)
            {
                board[x][y] = false;
            }
        }

        try
        {
            return solveRecursively(0);
        }
        catch(NQueensBacktrackException e)
        {
            board = null;

            throw new NQueensNotSolvableException();
        }
    }

    private boolean[][] solveRecursively(int i) throws NQueensBacktrackException
    {
        if(i == n)
        {
            return board;
        }

        for(int y = 0; y < n; y++)
        {
            for(int x = 0; x < n; x++)
            {
                try
                {
                    if(!threatened(x, y))
                    {
                        board[x][y] = true;

                        return solveRecursively(i + 1);
                    }
                }
                catch(NQueensBacktrackException e)
                {
                    board[x][y] = false;
                }
            }
        }

        throw new NQueensBacktrackException();
    }

    private boolean threatened(int x, int y)
    {
        for(int x0 = 0; x0 < n; x0++)
        {
            if(board[x0][y])
            {
                return true;
            }
        }

        for(int y0 = 0; y0 < n; y0++)
        {
            if(board[x][y0])
            {
                return true;
            }
        }

        for(int x0 = x, y0 = y; x0 < n && y0 < n; x0++, y0++)
        {
            if(board[x0][y0])
            {
                return true;
            }
        }

        for(int x0 = x, y0 = y; x0 < n && y0 >= 0; x0++, y0--)
        {
            if(board[x0][y0])
            {
                return true;
            }
        }

        for(int x0 = x, y0 = y; x0 >= 0 && y0 < n; x0--, y0++)
        {
            if(board[x0][y0])
            {
                return true;
            }
        }

        for(int x0 = x, y0 = y; x0 >= 0 && y0 >= 0; x0--, y0--)
        {
            if(board[x0][y0])
            {
                return true;
            }
        }

        return false;
    }

    public void printBoard()
    {
        StringBuilder stringBuilder = new StringBuilder();

        for(int y = 0; y < n; y++)
        {
            for(int x = 0; x < n; x++)
            {
                stringBuilder.append(board[x][y] ? "1 " : "0 ");
            }

            stringBuilder.append("\n");
        }

        System.out.println("Possible solution for n=" + n + "\n" + stringBuilder.toString());
    }
}
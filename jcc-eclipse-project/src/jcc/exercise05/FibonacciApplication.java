package jcc.exercise05;

public class FibonacciApplication
{
    public static int n = 42;

    public static void main(String[] args)
    {
        /*
         * 1 second = 1000 milliseconds = 1.000.000 microseconds = 1.000.000.000 nano seconds
         */
        long timestampStart = 0L;
        long timestampEnd = 0L;

        long iterativeDuration = 0L;
        long recursiveDuration = 0L;

        timestampStart = System.nanoTime();
        Fibonacci.fibonacciIterative(n);
        timestampEnd = System.nanoTime();

        iterativeDuration = timestampEnd - timestampStart;

        System.out.println("fibonacciIterative(42) took " + iterativeDuration + " nanoseconds to compute");

        timestampStart = System.nanoTime();
        Fibonacci.fibonacciRecursive(n);
        timestampEnd = System.nanoTime();

        recursiveDuration = timestampEnd - timestampStart;

        System.out.println("fibonacciRecursive(42) took " + recursiveDuration + " nanoseconds to compute");

        System.out.printf("Recursion took %.2f times longer than iteration\n", (double)recursiveDuration / iterativeDuration);
    }
}
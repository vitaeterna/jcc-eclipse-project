package jcc.exercise15;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class IntegerCalculatorFX extends Application
{
    public enum State
    {
        Number, Operator, EQsign
    }

    private String currentNumber = null;
    private String operator      = null;
    private State  state         = State.EQsign;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        TextField textField = new TextField();
        textField.alignmentProperty().set(Pos.CENTER_RIGHT);
        textField.setText("0");
        textField.editableProperty().set(false);
        textField.disableProperty().set(false);
        textField.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        Button button0 = new Button("0");
        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");
        Button button4 = new Button("4");
        Button button5 = new Button("5");
        Button button6 = new Button("6");
        Button button7 = new Button("7");
        Button button8 = new Button("8");
        Button button9 = new Button("9");
        Button buttonEQ = new Button("=");
        Button buttonADD = new Button("+");
        Button buttonSUB = new Button("-");
        Button buttonMUL = new Button("x");
        Button buttonDIV = new Button("/");
        Button buttonMOD = new Button("%");

        EventHandler<ActionEvent> eventHandlerNumber = new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                Button button = (Button)event.getSource();

                if(state == State.Number && !textField.getText().equals("0"))
                {
                    textField.setText(textField.getText() + button.getText());
                }
                else
                {
                    textField.setText(button.getText());
                }

                state = State.Number;
            }
        };

        EventHandler<ActionEvent> eventHandlerEQ = new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                if(state == State.Number)
                {
                    if(currentNumber != null && operator != null)
                    {
                        int number1 = 0;
                        int number2 = 0;
                        int number3 = 0;

                        try
                        {
                            number1 = Integer.parseInt(currentNumber);
                            number2 = Integer.parseInt(textField.getText());
                        }
                        catch(NumberFormatException e)
                        {
                            textField.setText("Number out of integer bounds");

                            currentNumber = null;
                            operator = null;

                            state = State.EQsign;

                            return;
                        }

                        if(operator.equals(buttonADD.getText()))
                        {
                            number3 = number1 + number2;
                        }
                        else if(operator.equals(buttonSUB.getText()))
                        {
                            number3 = number1 - number2;
                        }
                        else if(operator.equals(buttonMUL.getText()))
                        {
                            number3 = number1 * number2;
                        }
                        else if(operator.equals(buttonDIV.getText()))
                        {
                            if(number2 == 0)
                            {
                                textField.setText("Division by zero");

                                currentNumber = null;
                                operator = null;

                                state = State.EQsign;

                                return;
                            }

                            number3 = number1 / number2;
                        }
                        else if(operator.equals(buttonMOD.getText()))
                        {
                            if(number2 == 0)
                            {
                                textField.setText("Division by zero");

                                currentNumber = null;
                                operator = null;

                                state = State.EQsign;

                                return;
                            }

                            number3 = number1 % number2;
                        }
                        else
                        {
                            System.err.println("Unsupported operator found: " + operator);
                        }

                        textField.setText(String.valueOf(number3));

                        currentNumber = null;
                        operator = null;
                    }

                    state = State.EQsign;
                }
            }
        };

        EventHandler<ActionEvent> eventHandlerOperator = new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                Button button = (Button)event.getSource();

                if(state == State.Number)
                {
                    buttonEQ.fire();
                    currentNumber = textField.getText();
                }

                operator = button.getText();

                state = State.Operator;
            }
        };

        button0.setOnAction(eventHandlerNumber);
        button1.setOnAction(eventHandlerNumber);
        button2.setOnAction(eventHandlerNumber);
        button3.setOnAction(eventHandlerNumber);
        button4.setOnAction(eventHandlerNumber);
        button5.setOnAction(eventHandlerNumber);
        button6.setOnAction(eventHandlerNumber);
        button7.setOnAction(eventHandlerNumber);
        button8.setOnAction(eventHandlerNumber);
        button9.setOnAction(eventHandlerNumber);
        buttonEQ.setOnAction(eventHandlerEQ);
        buttonADD.setOnAction(eventHandlerOperator);
        buttonSUB.setOnAction(eventHandlerOperator);
        buttonMUL.setOnAction(eventHandlerOperator);
        buttonDIV.setOnAction(eventHandlerOperator);
        buttonMOD.setOnAction(eventHandlerOperator);

        button0.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button1.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button2.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button3.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button4.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button5.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button6.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button7.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button8.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        button9.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonEQ.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonADD.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonSUB.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonMUL.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonDIV.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        buttonMOD.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        ColumnConstraints columnConstraint = new ColumnConstraints();

        columnConstraint.setPercentWidth(25);
        columnConstraint.setHalignment(HPos.CENTER);
        columnConstraint.setHgrow(Priority.ALWAYS);

        RowConstraints rowConstraint = new RowConstraints();

        rowConstraint.setPercentHeight(20);
        rowConstraint.setValignment(VPos.CENTER);
        rowConstraint.setVgrow(Priority.ALWAYS);

        GridPane gridPane = new GridPane();
        gridPane.getColumnConstraints().addAll(columnConstraint, columnConstraint, columnConstraint, columnConstraint);
        gridPane.getRowConstraints().addAll(rowConstraint, rowConstraint, rowConstraint, rowConstraint, rowConstraint);
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.add(textField, 0, 0, 4, 1);
        gridPane.add(button7, 0, 1);
        gridPane.add(button8, 1, 1);
        gridPane.add(button9, 2, 1);
        gridPane.add(buttonADD, 3, 1);
        gridPane.add(button4, 0, 2);
        gridPane.add(button5, 1, 2);
        gridPane.add(button6, 2, 2);
        gridPane.add(buttonSUB, 3, 2);
        gridPane.add(button1, 0, 3);
        gridPane.add(button2, 1, 3);
        gridPane.add(button3, 2, 3);
        gridPane.add(buttonMUL, 3, 3);
        gridPane.add(button0, 0, 4);
        gridPane.add(buttonEQ, 1, 4);
        gridPane.add(buttonMOD, 2, 4);
        gridPane.add(buttonDIV, 3, 4);

        Scene scene = new Scene(gridPane, 400, 400);

        primaryStage.setTitle("IntegerCalculatorFX");
        primaryStage.setMinWidth(300);
        primaryStage.setMinHeight(300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
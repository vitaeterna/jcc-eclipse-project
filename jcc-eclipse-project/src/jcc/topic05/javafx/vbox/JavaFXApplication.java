package jcc.topic05.javafx.vbox;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFXApplication extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");

        VBox vbox = new VBox();
        vbox.getChildren().add(button1);
        vbox.getChildren().add(button2);
        vbox.getChildren().add(button3);

        Scene scene = new Scene(vbox);

        primaryStage.setTitle("VBox");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
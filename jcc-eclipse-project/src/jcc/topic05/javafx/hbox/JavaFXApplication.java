package jcc.topic05.javafx.hbox;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class JavaFXApplication extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");

        HBox hbox = new HBox();
        hbox.getChildren().add(button1);
        hbox.getChildren().add(button2);
        hbox.getChildren().add(button3);

        hbox.setSpacing(10);

        Scene scene = new Scene(hbox);

        primaryStage.setTitle("HBox");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
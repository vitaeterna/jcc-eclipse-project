package jcc.topic05.javafx.borderpane;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class JavaFXApplication extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Button buttonTop = new Button("Top");
        Button buttonBottom = new Button("Bottom");
        Button buttonLeft = new Button("Left");
        Button buttonRight = new Button("Right");
        Button buttonCenter = new Button("Center");

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(buttonTop);
        borderPane.setBottom(buttonBottom);
        borderPane.setLeft(buttonLeft);
        borderPane.setRight(buttonRight);
        borderPane.setCenter(buttonCenter);

        Scene scene = new Scene(borderPane);

        primaryStage.setTitle("BorderPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
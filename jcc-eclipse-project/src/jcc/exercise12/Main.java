package jcc.exercise12;

public class Main
{
    public static volatile int sharedInt = 0;

    public static void main(String[] args)
    {
        //Custom Class
        Thread thread0 = new Thread(new MyThread());
        thread0.start();

        //Anonymous Class extending Thread
        Thread thread1 = new Thread()
        {
            @Override
            public void run()
            {
                Main.sharedInt++;
            }
        };

        thread1.start();

        //Anonymous Class implementing Runnable
        Thread thread2 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                Main.sharedInt++;
            }
        });

        thread2.start();

        //Lambda Expression
        Thread thread3 = new Thread(() -> Main.sharedInt++);
        thread3.start();

        try
        {
            thread0.join();
            thread1.join();
            thread2.join();
            thread3.join();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        System.out.println(sharedInt);
    }
}
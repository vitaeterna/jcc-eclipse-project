package jcc.topic01.object;

public class DummyClass
{
    public int        dummyIntInstanceField = 1;

    public static int dummyIntClassField    = 2;

    public DummyClass()
    {
        dummyIntInstanceField = 1;
    }

    public DummyClass(int dummyInt)
    {
        dummyIntInstanceField = dummyInt;
    }

    public void dummyInstanceMethod()
    {
        System.out.println("I am an instance method. You need to create an instance before invoking.");
    }

    public static void dummyClassMethod()
    {
        System.out.println("I am a class method. You can invoke me without creating an instance.");
    }
}
package jcc.topic01.object;

public class Main
{
    public static int dummyIntField = 0;

    public static void main(String[] args)
    {
        //Static
        System.out.println(DummyClass.dummyIntClassField);

        DummyClass.dummyClassMethod();

        //Instance - empty constructor
        DummyClass object1 = new DummyClass();

        System.out.println(object1.dummyIntInstanceField);

        object1.dummyInstanceMethod();

        //Instance - parameterized constructor
        DummyClass object2 = new DummyClass(5);

        System.out.println(object2.dummyIntInstanceField);

        object2.dummyInstanceMethod();
    }
}
package jcc.topic01.inheritance;

public class Main
{
    public static void main(String[] args)
    {
        DummyClass object1 = new DummyClass();

        System.out.println(object1.superField);

        object1.superMethod1();
        object1.superMethod2();

        DummyClass2 object2 = new DummyClass2();

        System.out.println(object2.superField);
        System.out.println(object2.subField);

        object2.superMethod1();
        object2.superMethod2();
    }
}
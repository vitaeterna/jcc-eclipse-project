package jcc.topic01.inheritance;

public class DummyClass
{
    public int superField = 1;

    public void superMethod1()
    {
        System.out.println("I am superMethod1, defined in DummyClass");
    }

    public void superMethod2()
    {
        System.out.println("I am superMethod2, defined in DummyClass");
    }
}
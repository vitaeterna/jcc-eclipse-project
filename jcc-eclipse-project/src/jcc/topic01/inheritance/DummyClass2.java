package jcc.topic01.inheritance;

public class DummyClass2 extends DummyClass
{
    public int subField = 2;

    @Override
    public void superMethod1()
    {
        System.out.println("I am superMethod1, defined in DummyClass2 overriding superMethod1 in DummyClass");
    }
}
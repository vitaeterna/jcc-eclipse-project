package jcc.topic01.interfaces;

public class Main
{
    public static void main(String[] args)
    {
        DummyInterface dummy = null;

        dummy = new DummyClass();
        dummy.printClassName();

        dummy = new DummyClass2();
        dummy.printClassName();
    }
}
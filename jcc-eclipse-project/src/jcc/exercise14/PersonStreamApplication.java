package jcc.exercise14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.bind.config.PropertyOrderStrategy;

import jcc.exercise13.Person;

public class PersonStreamApplication
{
    public static void main(String[] args)
    {
        ArrayList<Person> persons = readPersonsFromJSON("./data/the_three_investigators.json");

        //1. Oldest Person
        System.out.println("1. Oldest Person");
        Person person = persons.stream().max((Person person1, Person person2) -> person1.getAge() - person2.getAge()).get();
        System.out.println(person);

        //2. Persons younger than 30
        System.out.println("\n2. Persons younger than 30");
        persons.stream().filter((Person person0) -> person0.getAge() < 30).forEach(System.out::println);

        //3. Persons having the surname "Jonas" sorted by age in ascending order
        System.out.println("\n3. Persons having the surname \"Jonas\" sorted by age in ascending order");
        persons.stream().filter((Person person0) -> person0.getSurName().equals("Jonas")).sorted((Person person1, Person person2) -> person1.getAge() - person2.getAge()).forEach(System.out::println);

        //4. Sum of all ages
        System.out.println("\n4. Sum of all ages");
        int ageSum = persons.stream().mapToInt(Person::getAge).sum();
        System.out.println(ageSum);

        //5. Distinct surnames sorted in ascending order
        System.out.println("\n5. Distinct surnames sorted in ascending order");
        persons.stream().map(Person::getSurName).sorted().distinct().forEach(System.out::println);

        //6. Number of distinct surnames
        System.out.println("\n6. Number of distinct surnames");
        long count = persons.stream().map(Person::getSurName).sorted().distinct().count();
        System.out.println(count);

        //7.Sums of ages grouped by surnames
        System.out.println("\n7. Sums of ages grouped by surnames");
        persons.stream().collect(Collectors.groupingBy(Person::getSurName, Collectors.summingInt(Person::getAge))).forEach((String string, Integer integer) -> System.out.println(string + " = " + integer));

        //8.Sums of ages grouped by surnames sorted by sums of ages in descending order
        System.out.println("\n8.Sums of ages grouped by surnames sorted by sums of ages in descending order");
        persons.stream().collect(Collectors.groupingBy(Person::getSurName, Collectors.summingInt(Person::getAge))).entrySet().stream().sorted((Entry<String, Integer> entry1, Entry<String, Integer> entry2) -> entry2.getValue() - entry1.getValue()).forEach(System.out::println);
    }

    public static ArrayList<Person> readPersonsFromJSON(String fileName)
    {
        StringBuilder stringBuilder = new StringBuilder();

        //Read JSON String from file
        try(Stream<String> stream = Files.lines(Paths.get(fileName)))
        {
            stream.forEach(stringBuilder::append);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        String jsonString = stringBuilder.toString();

        //JSON String to Java Objects
        JsonbConfig config = new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES).withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL).withFormatting(true);

        Jsonb jsonb = JsonbBuilder.create(config);

        ArrayList<Person> persons = jsonb.fromJson(jsonString, new ArrayList<Person>()
        {
            private static final long serialVersionUID = 1L;
        }.getClass().getGenericSuperclass());

        return persons;
    }
}
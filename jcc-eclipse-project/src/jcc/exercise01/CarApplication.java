package jcc.exercise01;

public class CarApplication
{
    public static void main(String[] args)
    {
        Car car = new Car();

        car.setBrand("VW");
        car.setType("Golf GTI");
        car.setHpNumber(230);

        car.setFuelConsumption(6.3d);
        car.setFuelTankSize(50);
        car.refuel();

        car.drive(100.0d);
        car.drive(1000.0d);
        car.drive(100.0d);
        car.refuel();
        car.drive(100.0d);

        car.setMaximumCargoWeight(40);

        Cargo cargo1 = new Cargo();
        cargo1.setWeight(30);

        Cargo cargo2 = new Cargo();
        cargo2.setWeight(35);

        Cargo cargo3 = new Cargo();
        cargo3.setWeight(45);

        car.load(cargo1);
        car.load(cargo2);
        car.unload();
        car.load(cargo3);
    }
}
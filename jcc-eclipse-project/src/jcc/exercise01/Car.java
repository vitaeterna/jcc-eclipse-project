package jcc.exercise01;

public class Car
{
    private String brand              = null;
    private String type               = null;
    private int    hpNumber           = 0;

    private double fuelConsumption    = 0.0d;
    private double fuelTankSize       = 0.0d;
    private double fuel               = 0.0d;

    private int    maximumCargoWeight = 0;
    private Cargo  cargo              = null;

    public void drive(double distance)
    {
        if(distance < 0.0d)
        {
            System.out.println("Negative distances not allowed!");
        }
        else
        {
            double fuelNeeded = distance / 100.0d * fuelConsumption;

            if(fuelNeeded > fuel)
            {
                System.out.printf("The car has driven %.2f km\n", fuel / fuelConsumption * 100.0d);

                fuel = 0.0d;
            }
            else
            {
                fuel = fuel - fuelNeeded;

                System.out.printf("The car has driven %.2f km\n", distance);
            }
        }
    }

    public void refuel()
    {
        setFuel(fuelTankSize);
    }

    public void load(Cargo cargo)
    {
        if(cargo == null)
        {
            return;
        }

        if(this.cargo == null)
        {
            int weight = cargo.getWeight();

            if(weight <= maximumCargoWeight)
            {
                this.cargo = cargo;

                System.out.println("Cargo loaded (" + weight + ")");
            }
            else
            {
                System.out.println("Cargo is too heavy! " + weight + " > " + maximumCargoWeight);
            }
        }
        else
        {
            System.out.println("Cargo already loaded!");
        }
    }

    public void unload()
    {
        System.out.println("Cargo unloaded (" + cargo.getWeight() + ")");

        cargo = null;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setHpNumber(int hpNumber)
    {
        if(hpNumber < 0)
        {
            this.hpNumber = 0;
        }
        else
        {
            this.hpNumber = hpNumber;
        }
    }

    public int getHpNumber()
    {
        return hpNumber;
    }

    public void setFuelConsumption(double fuelConsumption)
    {
        if(fuelConsumption < 0.0d)
        {
            this.fuelConsumption = 0.0d;
        }
        else
        {
            this.fuelConsumption = fuelConsumption;
        }
    }

    public double getFuelConsuption()
    {
        return fuelConsumption;
    }

    public void setFuelTankSize(double fuelTankSize)
    {
        if(fuelTankSize < 0.0d)
        {
            this.fuelTankSize = 0.0d;
        }
        else
        {
            this.fuelTankSize = fuelTankSize;
        }
    }

    public double getFuelTankSize()
    {
        return fuelTankSize;
    }

    public void setFuel(double fuel)
    {
        if(fuel < 0.0d)
        {
            this.fuel = 0.0d;
        }
        else if(fuel > fuelTankSize)
        {
            this.fuel = fuelTankSize;
        }
        else
        {
            this.fuel = fuel;
        }
    }

    public double getFuel()
    {
        return fuel;
    }

    public void setMaximumCargoWeight(int maximumCargoWeight)
    {
        if(maximumCargoWeight < 0)
        {
            this.maximumCargoWeight = 0;
        }
        else
        {
            this.maximumCargoWeight = maximumCargoWeight;
        }
    }

    public int getMaximumCargoWeight()
    {
        return maximumCargoWeight;
    }

    public Cargo getCargo()
    {
        return cargo;
    }
}
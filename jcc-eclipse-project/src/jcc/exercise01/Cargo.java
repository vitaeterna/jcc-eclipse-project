package jcc.exercise01;

public class Cargo
{
    private int weight = 0;

    public Cargo()
    {

    }

    public Cargo(int weight)
    {
        setWeight(weight);
    }

    public void setWeight(int weight)
    {
        if(weight < 0)
        {
            this.weight = 0;
        }
        else
        {
            this.weight = weight;
        }
    }

    public int getWeight()
    {
        return weight;
    }

    public void method1()
    {

    }

    public void method2()
    {

    }

    public void voidMethod()
    {

    }
}
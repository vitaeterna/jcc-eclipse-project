package jcc.topic07.unitTesting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import jcc.exercise01.Car;
import jcc.exercise01.Cargo;

public class CarTest
{
    @Test
    public void successfulLoadingTest()
    {
        Cargo cargo = Mockito.mock(Cargo.class);

        Mockito.when(cargo.getWeight()).thenReturn(80);

        Car car = new Car();
        car.setMaximumCargoWeight(100);
        car.load(cargo);

        Mockito.verify(cargo).getWeight();

        Assertions.assertSame(car.getCargo(), cargo);
    }

    @Test
    public void unsuccessfulLoadingTest()
    {
        Cargo cargo = Mockito.mock(Cargo.class);

        Mockito.when(cargo.getWeight()).thenReturn(120);

        Car car = new Car();
        car.setMaximumCargoWeight(100);
        car.load(cargo);

        Mockito.verify(cargo).getWeight();

        Assertions.assertNull(car.getCargo());
    }
}
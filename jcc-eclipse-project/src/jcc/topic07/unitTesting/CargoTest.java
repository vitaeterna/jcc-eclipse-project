package jcc.topic07.unitTesting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import jcc.exercise01.Cargo;

public class CargoTest
{
    private Cargo cargo = null;

    @BeforeEach
    public void init()
    {
        cargo = new Cargo();
    }

    @Test
    public void constructorShouldSetWeight()
    {
        Cargo cargo = new Cargo(45);

        assertEquals(cargo.getWeight(), 45);
    }

    @Test
    public void constructorShouldSetWeight2()
    {
        Cargo cargo = new Cargo(45);

        assertEquals(cargo.getWeight(), 45);

        cargo = new Cargo(60);

        assertEquals(cargo.getWeight(), 60);
    }

    @Test
    public void constructorShouldSetWeight3()
    {
        int[] values = { 45, 60 };

        for(int index = 0; index < values.length; index++)
        {
            Cargo cargo = new Cargo(values[index]);

            assertEquals(cargo.getWeight(), values[index]);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = { 45, 60 })
    public void constructorShouldSetWeight4(int weight)
    {
        Cargo cargo = new Cargo(weight);

        assertEquals(cargo.getWeight(), weight);
    }

    @Test
    public void setterShouldSetWeight()
    {
        cargo.method1();

        assertEquals(cargo.getWeight(), 0);
    }

    @Test
    public void setterShouldSetWeight2()
    {
        cargo.method2();

        assertEquals(cargo.getWeight(), 0);
    }

    @Test
    public void mockito()
    {
        Cargo cargo = Mockito.mock(Cargo.class);

        Mockito.when(cargo.getWeight()).thenReturn(0);
        Mockito.when(cargo.getWeight()).thenThrow(new RuntimeException());

        Mockito.doNothing().when(cargo).voidMethod();
        Mockito.doThrow(new RuntimeException()).when(cargo).voidMethod();

        cargo.setWeight(50);
        assertThrows(RuntimeException.class, () -> cargo.getWeight());

        Mockito.verify(cargo).setWeight(50);
        Mockito.verify(cargo).getWeight();
    }
}
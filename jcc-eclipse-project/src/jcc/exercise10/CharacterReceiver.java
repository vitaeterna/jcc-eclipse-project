package jcc.exercise10;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class CharacterReceiver
{
    public static void main(String[] args)
    {
        try
        {
            @SuppressWarnings("resource")
            DatagramSocket socket = new DatagramSocket(50000);
            DatagramPacket packet = null;

            while(true)
            {
                packet = new DatagramPacket(new byte[1024], 1024);

                socket.receive(packet);

                System.out.print(new String(packet.getData(), 0, packet.getLength()));
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
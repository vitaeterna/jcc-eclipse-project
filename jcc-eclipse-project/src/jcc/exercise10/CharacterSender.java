package jcc.exercise10;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class CharacterSender
{
    public static void main(String[] args) throws UnknownHostException
    {
        InetAddress receiverAddress = InetAddress.getByName("127.0.0.1");
        int receiverPort = 50000;

        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);

        while(true)
        {
            System.out.println("Please insert a message:");

            String input = scanner.nextLine() + '\n';

            try
            {
                DatagramSocket socket = new DatagramSocket();

                DatagramPacket packet = null;

                byte[] data = null;

                for(int index = 0; index < input.length(); index++)
                {
                    data = String.valueOf(input.charAt(index)).getBytes();

                    packet = new DatagramPacket(data, data.length, receiverAddress, receiverPort);

                    socket.send(packet);
                }

                socket.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            System.out.println("Message sent\n");
        }
    }
}
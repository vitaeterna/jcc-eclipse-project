package jcc.exercise04;

public class ShoppingCartArrayImpl implements ShoppingCart
{
    private Article[] articles     = new Article[1];
    private int       articleCount = 0;

    public ShoppingCartArrayImpl()
    {
        System.out.println("ShoppingCartArrayImpl");
    }

    @Override
    public void add(Article article)
    {
        if(article == null)
        {
            return;
        }

        if(articleCount < articles.length)
        {
            articles[articleCount] = article;

            articleCount++;
        }
        else
        {
            System.out.println("Array too small! " + articles.length + " -> " + articles.length * 2);

            Article[] newArticleArray = new Article[articles.length * 2];

            for(int index = 0; index < articles.length; index++)
            {
                newArticleArray[index] = articles[index];
            }

            articles = newArticleArray;

            add(article);
        }
    }

    @Override
    public double calculateTotalPrice()
    {
        double totalPrice = 0.0d;

        for(int index = 0; index < articleCount; index++)
        {
            totalPrice += articles[index].getPrice();
        }

        return totalPrice;
    }

    @Override
    public String toString()
    {
        String tempString = "";

        for(int index = 0; index < articleCount; index++)
        {
            tempString += articles[index].toString() + "\n";
        }

        return tempString;
    }
}
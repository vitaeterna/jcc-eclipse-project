package jcc.exercise04;

import java.util.ArrayList;

public class ShoppingCartListImpl implements ShoppingCart
{
    private ArrayList<Article> articles = new ArrayList<>();

    public ShoppingCartListImpl()
    {
        System.out.println("ShoppingCartListImpl");
    }

    @Override
    public void add(Article article)
    {
        if(article == null)
        {
            return;
        }

        articles.add(article);
    }

    @Override
    public double calculateTotalPrice()
    {
        double totalPrice = 0.0d;

        for(Article article : articles)
        {
            totalPrice += article.getPrice();
        }

        return totalPrice;
    }

    @Override
    public String toString()
    {
        String tempString = "";

        for(Article article : articles)
        {
            tempString += article.toString() + "\n";
        }

        return tempString;
    }
}
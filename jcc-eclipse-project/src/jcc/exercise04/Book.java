package jcc.exercise04;

public class Book extends Article
{
    private String title = null;

    public Book()
    {
        setTitle("Default Title");
        setPrice(0.0d);
    }

    public Book(String title, double price)
    {
        setTitle(title);
        setPrice(price);
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    @Override
    public String toString()
    {
        return "Book - Title: \"" + getTitle() + "\" Price: " + getPrice();
    }
}
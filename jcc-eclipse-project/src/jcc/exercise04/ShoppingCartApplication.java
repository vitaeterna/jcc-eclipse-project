package jcc.exercise04;

public class ShoppingCartApplication
{
    public static void main(String[] args)
    {
        Book book1 = new Book("Head First Java", 29.99d);

        Book book2 = new Book("Effective Java", 51.99d);

        Book book3 = new Book("Java Cookbook", 35.99d);

        CD cd1 = new CD("Bruno Mars", 9.99d);

        CD cd2 = new CD("Sam Smith", 12.99d);

        CD cd3 = new CD("P!nk", 11.99d);

        //ShoppingCart shoppingCart = new ShoppingCartListImpl();
        ShoppingCart shoppingCart = new ShoppingCartArrayImpl();
        shoppingCart.add(book1);
        shoppingCart.add(book2);
        shoppingCart.add(book3);
        shoppingCart.add(cd1);
        shoppingCart.add(cd2);
        shoppingCart.add(cd3);

        System.out.print(shoppingCart.toString());

        System.out.println(shoppingCart.calculateTotalPrice());
    }
}
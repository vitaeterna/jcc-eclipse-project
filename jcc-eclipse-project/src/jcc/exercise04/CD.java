package jcc.exercise04;

public class CD extends Article
{
    private String interpreter = null;

    public CD()
    {
        setInterpreter("Default Interpreter");
        setPrice(0.0d);
    }

    public CD(String interpreter, double price)
    {
        setInterpreter(interpreter);
        setPrice(price);
    }

    public void setInterpreter(String interpreter)
    {
        this.interpreter = interpreter;
    }

    public String getInterpreter()
    {
        return interpreter;
    }

    @Override
    public String toString()
    {
        return "CD - Interpreter: \"" + getInterpreter() + "\" Price: " + getPrice();
    }
}
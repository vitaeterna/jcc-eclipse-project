package jcc.exercise04;

public interface ShoppingCart
{
    public void add(Article article);

    public double calculateTotalPrice();
}
package jcc.topic04.json.bind;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;

public class Person
{
    @JsonbProperty("name")
    private String name = null;
    @JsonbTransient
    private int    age  = -1;

    public Person()
    {

    }

    public Person(String name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public int getAge()
    {
        return age;
    }

    @Override
    public String toString()
    {
        return "(" + name + "/" + age + ")";
    }
}
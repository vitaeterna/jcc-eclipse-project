package jcc.topic04.json.bind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.bind.config.PropertyOrderStrategy;

public class Main
{
    public static void main(String[] args)
    {
        //Create Java Objects
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Peter", 18));
        persons.add(new Person("Anna", 31));
        persons.add(new Person("Mike", 28));
        persons.add(new Person("Xenia", 18));

        for(Person person : persons)
        {
            System.out.println(person);
        }

        //Java Objects to JSON String
        JsonbConfig config = new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES).withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL).withFormatting(false);

        Jsonb jsonb = JsonbBuilder.create(config);

        String jsonString = jsonb.toJson(persons);

        System.out.println("\n" + jsonString + "\n");

        //JSON String to Java Objects
        ArrayList<Person> persons2 = jsonb.fromJson(jsonString, new ArrayList<Person>()
        {
            private static final long serialVersionUID = 1L;
        }.getClass().getGenericSuperclass());

        for(Person person : persons2)
        {
            System.out.println(person);
        }

        List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");

        myList.stream().filter(s -> s.startsWith("c")).map(String::toUpperCase).sorted();
    }
}
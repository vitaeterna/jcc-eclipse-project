package jcc.topic04.json.parse;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

public class Main
{
    public static void main(String[] args) throws FileNotFoundException
    {
        //Create Java Objects
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Peter", 18));
        persons.add(new Person("Anna", 31));
        persons.add(new Person("Mike", 28));
        persons.add(new Person("Xenia", 18));

        for(Person person : persons)
        {
            System.out.println(person);
        }

        //Java Objects to JSON String
        String jsonString = writeManually(persons);

        System.out.println(jsonString + "\n");

        //JSON String to Java Objects
        ArrayList<Person> persons2 = readManually(jsonString);

        for(Person person : persons2)
        {
            System.out.println(person);
        }
    }

    private static String writeManually(ArrayList<Person> persons)
    {
        //Create JSON Objects from Java Objects
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        JsonObjectBuilder objectBuilder = null;
        JsonObject jsonObject = null;
        JsonArray jsonArray = null;

        for(Person person : persons)
        {
            objectBuilder = Json.createObjectBuilder();
            objectBuilder.add("name", person.getName());
            objectBuilder.add("age", person.getAge());

            jsonObject = objectBuilder.build();

            arrayBuilder.add(jsonObject);
        }

        jsonArray = arrayBuilder.build();

        //Create JSON String from JSON Objects
        Map<String, Boolean> config = new HashMap<>();
        config.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(config);

        Writer writer = new StringWriter();
        writerFactory.createWriter(writer).write(jsonArray);

        String jsonString = writer.toString();

        return jsonString;
    }

    private static ArrayList<Person> readManually(String jsonString)
    {
        //JsonReader reader = Json.createReader(new FileInputStream("persons.json"));
        JsonReader reader = Json.createReader(new StringReader(jsonString));

        JsonArray jsonArray = reader.readArray();

        ArrayList<Person> persons2 = new ArrayList<>();
        JsonObject jsonObject = null;
        String name = null;
        int age = -1;

        for(int index = 0; index < jsonArray.size(); index++)
        {
            jsonObject = jsonArray.getJsonObject(index);

            name = jsonObject.getString("name");
            age = jsonObject.getInt("age");

            persons2.add(new Person(name, age));
        }

        return persons2;
    }
}
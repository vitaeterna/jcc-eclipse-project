package jcc.topic04.stream;

import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        Stream.of("a4", "d2", "a2", "b1", "b3", "c", "a1").filter(s ->
        {
            System.out.println("filter: " + s);
            return s.startsWith("a");
        }).sorted((s1, s2) ->
        {
            System.out.println("sorted: " + s1 + ", " + s2);
            return s1.compareTo(s2);
        }).map(s ->
        {
            System.out.println("map: " + s);
            return s.toUpperCase();
        }).forEach(s -> System.out.println("forEach: " + s));
    }
}
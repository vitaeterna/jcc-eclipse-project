package jcc.topic02.threading.deadlock;

public class MyThread12 extends Thread
{
    @Override
    public void run()
    {
        synchronized(Main.monitor1)
        {
            synchronized(Main.monitor2)
            {

            }
        }
    }
}
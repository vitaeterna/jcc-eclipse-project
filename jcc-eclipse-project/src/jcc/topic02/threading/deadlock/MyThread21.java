package jcc.topic02.threading.deadlock;

public class MyThread21 extends Thread
{
    @Override
    public void run()
    {
        synchronized(Main.monitor2)
        {
            synchronized(Main.monitor1)
            {

            }
        }
    }
}
package jcc.topic02.threading.deadlock;

public class Main
{
    public static Object monitor1 = new Object();
    public static Object monitor2 = new Object();

    public static void main(String[] args)
    {
        Thread thread12 = new MyThread12();
        Thread thread21 = new MyThread21();

        thread12.start();
        thread21.start();

        try
        {
            thread12.join();
            thread21.join();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
package jcc.topic02.threading.synchronizedBlock;

public class MyThread extends Thread
{
    @Override
    public void run()
    {
        synchronized(Main.monitor)
        {
            Main.sharedInt++;
        }
    }
}
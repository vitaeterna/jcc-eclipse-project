package jcc.topic02.threading.synchronizedBlock;

public class Main
{
    public static int    sharedInt = 0;
    public static Object monitor   = new Object();

    public static void main(String[] args)
    {
        Thread thread1 = new MyThread();
        Thread thread2 = new MyThread();

        thread1.start();
        thread2.start();

        try
        {
            thread1.join();
            thread2.join();

            System.out.println(sharedInt);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
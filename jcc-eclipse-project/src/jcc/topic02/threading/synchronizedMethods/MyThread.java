package jcc.topic02.threading.synchronizedMethods;

public class MyThread extends Thread
{
    @Override
    public void run()
    {
        Main.mySharedInt.increment();
    }
}
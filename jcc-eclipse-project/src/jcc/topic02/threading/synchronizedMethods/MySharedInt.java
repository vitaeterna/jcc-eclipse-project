package jcc.topic02.threading.synchronizedMethods;

public class MySharedInt
{
    public int value = 0;

    public synchronized void increment()
    {
        value++;
    }
}
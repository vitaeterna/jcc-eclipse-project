package jcc.topic02.threading.synchronizedMethods;

public class Main
{
    public static MySharedInt mySharedInt = new MySharedInt();

    public static void main(String[] args)
    {
        Thread thread1 = new MyThread();
        Thread thread2 = new MyThread();

        thread1.start();
        thread2.start();

        try
        {
            thread1.join();
            thread2.join();

            System.out.println(mySharedInt.value);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
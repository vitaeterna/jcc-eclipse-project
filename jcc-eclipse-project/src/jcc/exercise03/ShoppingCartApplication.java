package jcc.exercise03;

public class ShoppingCartApplication
{
    public static void main(String[] args)
    {
        Book book1 = new Book();
        book1.setTitle("Head First Java");
        book1.setPrice(29.99d);

        Book book2 = new Book();
        book2.setTitle("Effective Java");
        book2.setPrice(51.99d);

        Book book3 = new Book();
        book3.setTitle("Java Cookbook");
        book3.setPrice(35.99d);

        CD cd1 = new CD();
        cd1.setInterpreter("Bruno Mars");
        cd1.setPrice(9.99d);

        CD cd2 = new CD();
        cd2.setInterpreter("Sam Smith");
        cd2.setPrice(12.99d);

        CD cd3 = new CD();
        cd3.setInterpreter("P!nk");
        cd3.setPrice(11.99d);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.add(book1);
        shoppingCart.add(book2);
        shoppingCart.add(book3);
        shoppingCart.add(cd1);
        shoppingCart.add(cd2);
        shoppingCart.add(cd3);

        System.out.println(shoppingCart.calculateTotalPrice());
    }
}
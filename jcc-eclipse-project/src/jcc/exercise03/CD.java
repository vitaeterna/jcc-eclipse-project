package jcc.exercise03;

public class CD extends Article
{
    private String interpreter = null;

    public void setInterpreter(String interpreter)
    {
        this.interpreter = interpreter;
    }

    public String getInterpreter()
    {
        return interpreter;
    }
}
package jcc.exercise03;

public class Book extends Article
{
    private String title = null;

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
}
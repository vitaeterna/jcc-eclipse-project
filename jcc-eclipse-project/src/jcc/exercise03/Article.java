package jcc.exercise03;

public class Article
{
    private double price = 0.0d;

    public void setPrice(double price)
    {
        if(price < 0)
        {
            this.price = 0.0d;
        }
        else
        {
            this.price = price;
        }
    }

    public double getPrice()
    {
        return price;
    }
}
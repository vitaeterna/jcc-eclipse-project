package jcc.exercise03;

import java.util.ArrayList;

public class ShoppingCart
{
    private ArrayList<Article> articles = new ArrayList<>();

    public void add(Article article)
    {
        if(article == null)
        {
            return;
        }

        articles.add(article);
    }

    public double calculateTotalPrice()
    {
        double totalPrice = 0.0d;

        for(Article article : articles)
        {
            totalPrice += article.getPrice();
        }

        return totalPrice;
    }
}
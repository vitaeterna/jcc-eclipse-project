package jcc.topic03.networking.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

public class Sender
{
    public static void main(String[] args)
    {
        byte[] data = "Hello World".getBytes();

        try
        {
            while(true)
            {
                DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName("127.0.0.1"), 50000);

                DatagramSocket socket = new DatagramSocket();
                socket.send(packet);
                socket.close();

                TimeUnit.SECONDS.sleep(1);
            }
        }
        catch(IOException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}

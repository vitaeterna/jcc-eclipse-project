package jcc.topic03.networking.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Receiver
{
    public static void main(String[] args)
    {
        try
        {
            @SuppressWarnings("resource")
            DatagramSocket socket = new DatagramSocket(50000);

            while(true)
            {
                DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
                socket.receive(packet);

                System.out.println("ip address: " + packet.getAddress());
                System.out.println("port:       " + packet.getPort());
                System.out.println("length:     " + packet.getLength());
                System.out.println("data:       " + new String(packet.getData(), 0, packet.getLength()) + "\n");
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}

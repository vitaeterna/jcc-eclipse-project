package jcc.topic03.networking.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{
    public static void main(String[] args)
    {
        try
        {
            @SuppressWarnings("resource")
            ServerSocket serverSocket = new ServerSocket(50000);

            while(true)
            {
                Socket clientSocket = serverSocket.accept();

                System.out.println("Server: " + clientSocket.getLocalAddress() + ":" + clientSocket.getLocalPort());
                System.out.println("Client: " + clientSocket.getInetAddress() + ":" + clientSocket.getPort());

                new ConnectionThread(clientSocket).start();
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
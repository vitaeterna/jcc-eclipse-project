package jcc.topic03.networking.tcp;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionThread extends Thread
{
    private Socket clientSocket = null;

    public ConnectionThread(Socket clientSocket)
    {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run()
    {
        try
        {
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
            writer.println("Hello World");
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
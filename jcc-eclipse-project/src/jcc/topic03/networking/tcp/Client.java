package jcc.topic03.networking.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client
{
    public static void main(String[] args)
    {
        try
        {
            Socket socket = new Socket("127.0.0.1", 50000);

            System.out.println(socket.getLocalPort());

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println(reader.readLine());

            socket.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
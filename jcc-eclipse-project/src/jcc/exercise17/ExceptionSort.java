package jcc.exercise17;

public class ExceptionSort
{
    private int[] intArray = null;

    public ExceptionSort(int[] intArray)
    {
        this.intArray = intArray;
    }

    public void sort()
    {
        try
        {
            sortRecursively(0, intArray.length, 0, 0);
        }
        catch(SelectionFinishedException e)
        {
            e.printStackTrace();
        }
    }

    private void sortRecursively(int index, int length, int indexToSelect, int minIndex) throws SelectionFinishedException
    {
        if(index == length)
        {
            System.out.println("min:" + intArray[minIndex] + " / minIndex:" + minIndex);

            throw new SelectionFinishedException(intArray[minIndex], minIndex);
        }

        if(index == indexToSelect)
        {
            System.out.println("Finding minimum for sort index:" + indexToSelect);

            minIndex = index;
        }
        else
        {
            System.out.print(intArray[minIndex] + " > " + intArray[index] + " ? -> ");

            if(intArray[minIndex] > intArray[index])
            {
                minIndex = index;
            }

            System.out.println(intArray[minIndex] + " index:" + minIndex);
        }

        while(true)
        {
            try
            {
                sortRecursively(index + 1, length, indexToSelect, minIndex);

                break;
            }
            catch(SelectionFinishedException e)
            {
                System.out.print("Exception on index:" + index);

                if(index == indexToSelect)
                {
                    System.out.println(" (indexToSort) set to " + intArray[e.getMinIndex()]);

                    intArray[index] = e.getMin();

                    indexToSelect++;

                    if(indexToSelect == length)
                    {
                        System.out.println("Sorting finished");

                        break;
                    }
                }
                else
                {
                    if(index == e.getMinIndex())
                    {
                        System.out.println(" (minIndex) set to " + intArray[indexToSelect]);

                        intArray[index] = intArray[indexToSelect];
                    }
                    else
                    {
                        System.out.print("\n");
                    }

                    throw e;
                }
            }
        }
    }
}
package jcc.exercise17;

import java.util.Arrays;

public class ExceptionSortApplication
{
    public static void main(String[] args)
    {
        int[] intArray = { 5, 9, 1, 4, 6, 7, 3, 2, 8, 0 };

        System.out.println(Arrays.toString(intArray));

        ExceptionSort exceptionSort = new ExceptionSort(intArray);
        exceptionSort.sort();

        System.out.println(Arrays.toString(intArray));
    }
}
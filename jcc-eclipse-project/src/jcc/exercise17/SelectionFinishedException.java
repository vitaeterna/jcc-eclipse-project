package jcc.exercise17;

public class SelectionFinishedException extends Exception
{
    private static final long serialVersionUID = 1L;

    private int               min              = 0;
    private int               minIndex         = 0;

    public SelectionFinishedException(int min, int minIndex)
    {
        this.min = min;
        this.minIndex = minIndex;
    }

    public int getMin()
    {
        return min;
    }

    public int getMinIndex()
    {
        return minIndex;
    }
}
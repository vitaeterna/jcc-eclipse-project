package jcc.exercise13;

import javax.json.bind.annotation.JsonbProperty;

public class Person
{
    @JsonbProperty("forename")
    private String forename = null;
    @JsonbProperty("surname")
    private String surname  = null;
    @JsonbProperty("age")
    private int    age      = -1;

    public Person()
    {

    }

    public Person(String forename, String surname, int age)
    {
        this.forename = forename;
        this.surname = surname;
        this.age = age;
    }

    public void setForeName(String forename)
    {
        this.forename = forename;
    }

    public String getForeName()
    {
        return forename;
    }

    public void setSurName(String surname)
    {
        this.surname = surname;
    }

    public String getSurName()
    {
        return surname;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public int getAge()
    {
        return age;
    }

    @Override
    public String toString()
    {
        return "(" + forename + " " + surname + " / " + age + ")";
    }
}
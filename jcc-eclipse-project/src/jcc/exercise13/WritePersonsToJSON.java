package jcc.exercise13;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.bind.config.PropertyOrderStrategy;

public class WritePersonsToJSON
{
    public static void main(String[] args)
    {
        //Create Java Objects
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person("Justus", "Jonas", 20));
        persons.add(new Person("Peter", "Shaw", 20));
        persons.add(new Person("Bob", "Andrews", 21));
        persons.add(new Person("Alfred", "Hitchcock", 118));
        persons.add(new Person("Albert", "Hitfield", 58));
        persons.add(new Person("Samuel", "Reynolds", 44));
        persons.add(new Person("Mathilda", "Jonas", 42));
        persons.add(new Person("Titus", "Jonas", 44));
        persons.add(new Person("Cathrine", "Jonas", 42));
        persons.add(new Person("Julius", "Jonas", 43));
        persons.add(new Person("Bill", "Andrews", 43));
        persons.add(new Person("Jelena", "Charkova", 18));
        persons.add(new Person("Victor", "Hugenay", 58));
        persons.add(new Person("Skinny", "Norris", 23));
        persons.add(new Person("Dick", "Perry", 49));
        persons.add(new Person("Clarissa", "Franklin", 35));
        persons.add(new Person("Wilbur", "Graham", 36));

        for(Person person : persons)
        {
            System.out.println(person);
        }

        //Java Objects to JSON String
        JsonbConfig config = new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES).withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL).withFormatting(true);

        Jsonb jsonb = JsonbBuilder.create(config);

        String jsonString = jsonb.toJson(persons);

        System.out.print(jsonString);

        //Write JSON String to file
        try(PrintWriter out = new PrintWriter("./data/the_three_investigators.json"))
        {
            out.print(jsonString);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
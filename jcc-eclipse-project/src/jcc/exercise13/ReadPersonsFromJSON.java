package jcc.exercise13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.bind.config.PropertyOrderStrategy;

public class ReadPersonsFromJSON
{
    public static void main(String[] args)
    {
        StringBuilder stringBuilder = new StringBuilder();

        //Read JSON String from file
        try(Stream<String> stream = Files.lines(Paths.get("./data/the_three_investigators.json")))
        {
            stream.forEach(stringBuilder::append);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        String jsonString = stringBuilder.toString();

        //JSON String to Java Objects
        JsonbConfig config = new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES).withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL).withFormatting(true);

        Jsonb jsonb = JsonbBuilder.create(config);

        ArrayList<Person> persons = jsonb.fromJson(jsonString, new ArrayList<Person>()
        {
            private static final long serialVersionUID = 1L;
        }.getClass().getGenericSuperclass());

        for(Person person : persons)
        {
            System.out.println(person);
        }
    }
}
package jcc.topic08.behavioral.strategy;

public class FibonacciRecursive implements FibonacciStrategy
{
    @Override
    public int calculate(int n)
    {
        if(n == 1 || n == 2)
        {
            return 1;
        }
        else
        {
            return calculate(n - 1) + calculate(n - 2);
        }
    }
}
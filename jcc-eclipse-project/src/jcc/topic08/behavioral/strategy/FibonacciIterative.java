package jcc.topic08.behavioral.strategy;

public class FibonacciIterative implements FibonacciStrategy
{
    @Override
    public int calculate(int n)
    {
        if(n == 1 || n == 2)
        {
            return 1;
        }
        else
        {
            int currentN = 3;
            int minus0 = 0;
            int minus1 = 1;
            int minus2 = 1;

            while(currentN <= n)
            {
                minus0 = minus1 + minus2;

                minus2 = minus1;
                minus1 = minus0;

                currentN++;
            }

            return minus0;
        }
    }
}
package jcc.topic08.behavioral.strategy;

public class FibonacciApplication
{
    public static void main(String[] args)
    {
        FibonacciContext context = new FibonacciContext(new FibonacciRecursive());
        System.out.println(context.calculate(42));

        context = new FibonacciContext(new FibonacciIterative());
        System.out.println(context.calculate(42));
    }
}
package jcc.topic08.behavioral.strategy;

public interface FibonacciStrategy
{
    public int calculate(int n);
}
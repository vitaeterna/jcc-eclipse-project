package jcc.topic08.behavioral.strategy;

public class FibonacciContext
{
    private FibonacciStrategy strategy = null;

    public FibonacciContext(FibonacciStrategy strategy)
    {
        this.strategy = strategy;
    }

    public int calculate(int n)
    {
        return strategy.calculate(n);
    }
}
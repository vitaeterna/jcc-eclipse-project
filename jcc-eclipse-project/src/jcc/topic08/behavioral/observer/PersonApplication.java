package jcc.topic08.behavioral.observer;

public class PersonApplication
{
    public static void main(String[] args)
    {
        Person person = new Person("James Bond");

        person.addObserver(new PersonObserver());

        person.setFullName("James Blond");
    }
}
package jcc.topic08.behavioral.observer;

import java.util.ArrayList;

public class MyObservable
{
    private ArrayList<MyObserver> observers = null;

    public MyObservable()
    {
        observers = new ArrayList<>();
    }

    public synchronized void addObserver(MyObserver observer)
    {
        if(observer == null)
        {
            throw new NullPointerException();
        }

        if(!observers.contains(observer))
        {
            observers.add(observer);
        }
    }

    public synchronized void deleteObserver(MyObserver observer)
    {
        observers.remove(observer);
    }

    public synchronized void deleteObservers()
    {
        observers.clear();
    }

    public void notifyObservers()
    {
        for(MyObserver observer : observers)
        {
            observer.update(this);
        }
    }
}
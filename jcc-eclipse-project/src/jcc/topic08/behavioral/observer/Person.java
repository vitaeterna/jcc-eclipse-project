package jcc.topic08.behavioral.observer;

public class Person extends MyObservable
{
    private String fullName = null;

    public Person(String fullName)
    {
        this.fullName = fullName;
    }

    public void setFullName(String fullName)
    {
        if(!this.fullName.equals(fullName))
        {
            this.fullName = fullName;

            notifyObservers();
        }
    }

    public String getFullName()
    {
        return fullName;
    }
}
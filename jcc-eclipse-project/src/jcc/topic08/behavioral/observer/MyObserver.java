package jcc.topic08.behavioral.observer;

public interface MyObserver
{
    public void update(MyObservable observable);
}
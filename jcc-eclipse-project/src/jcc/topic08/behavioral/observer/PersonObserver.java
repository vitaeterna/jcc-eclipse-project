package jcc.topic08.behavioral.observer;

public class PersonObserver implements MyObserver
{
    @Override
    public void update(MyObservable observable)
    {
        Person person = (Person)observable;

        System.out.println("Person changed: " + person.getFullName());
    }
}
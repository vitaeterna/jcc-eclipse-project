package jcc.topic08.structural.adapter;

//Target
public interface Customer
{
    public String getFirstName();

    public String getLastName();
}
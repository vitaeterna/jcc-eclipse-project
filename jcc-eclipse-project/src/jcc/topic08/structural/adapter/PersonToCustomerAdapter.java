package jcc.topic08.structural.adapter;

//Adapter
public class PersonToCustomerAdapter implements Customer
{
    private Person person = null;

    public PersonToCustomerAdapter(Person person)
    {
        this.person = person;
    }

    @Override
    public String getFirstName()
    {
        return person.getFullName().split(" ")[0];
    }

    @Override
    public String getLastName()
    {
        return person.getFullName().split(" ")[1];
    }
}
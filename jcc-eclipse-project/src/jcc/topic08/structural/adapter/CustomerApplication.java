package jcc.topic08.structural.adapter;

//Client
public class CustomerApplication
{
    public static void main(String[] args)
    {
        Person person = new Person("James Bond");

        Customer customer = new PersonToCustomerAdapter(person);

        System.out.println(customer.getFirstName());
        System.out.println(customer.getLastName());
    }
}
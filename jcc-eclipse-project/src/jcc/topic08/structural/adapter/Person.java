package jcc.topic08.structural.adapter;

//Adaptee
public class Person
{
    private String fullName = null;

    public Person(String fullName)
    {
        this.fullName = fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getFullName()
    {
        return fullName;
    }
}
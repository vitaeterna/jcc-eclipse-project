package jcc.topic08.creational.factory;

public class Square implements Shape
{
    @Override
    public void draw()
    {
        System.out.println("Square");
    }
}
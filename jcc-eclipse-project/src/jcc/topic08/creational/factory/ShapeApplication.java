package jcc.topic08.creational.factory;

import jcc.topic08.creational.factory.ShapeFactory.ShapeEnum;

public class ShapeApplication
{
    public static void main(String[] args)
    {
        Shape shape = null;

        shape = ShapeFactory.getShape(ShapeEnum.Circle);
        shape.draw();

        shape = ShapeFactory.getShape(ShapeEnum.Square);
        shape.draw();

        shape = ShapeFactory.getShape(ShapeEnum.Triangle);
        shape.draw();
    }
}
package jcc.topic08.creational.factory;

public interface Shape
{
    public void draw();
}
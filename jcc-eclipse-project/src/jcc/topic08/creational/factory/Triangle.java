package jcc.topic08.creational.factory;

public class Triangle implements Shape
{
    @Override
    public void draw()
    {
        System.out.println("Triangle");
    }
}
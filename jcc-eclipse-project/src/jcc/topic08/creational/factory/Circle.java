package jcc.topic08.creational.factory;

public class Circle implements Shape
{
    @Override
    public void draw()
    {
        System.out.println("Circle");
    }
}
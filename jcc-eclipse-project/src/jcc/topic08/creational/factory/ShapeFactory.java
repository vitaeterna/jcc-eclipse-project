package jcc.topic08.creational.factory;

public class ShapeFactory
{
    public enum ShapeEnum
    {
        Circle, Square, Triangle;
    }

    public static Shape getShape(ShapeEnum shape)
    {
        switch(shape)
        {
            case Circle:
                return new Circle();
            case Square:
                return new Square();
            case Triangle:
                return new Triangle();
            default:
                return null;
        }
    }
}
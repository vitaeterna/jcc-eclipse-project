package jcc.exercise06;

public class CollatzApplication
{
    public static CollatzNumber collatzNumber = new CollatzNumber(100);

    public static void main(String[] args)
    {
        CollatzEvenThread evenThread = new CollatzEvenThread();
        evenThread.setName("CollatzEvenThread");
        evenThread.start();

        CollatzOddThread oddThread = new CollatzOddThread();
        oddThread.setName("CollatzOddThread");
        oddThread.start();

        try
        {
            evenThread.join();
            oddThread.join();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
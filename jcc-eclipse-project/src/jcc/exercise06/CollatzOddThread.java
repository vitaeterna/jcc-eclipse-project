package jcc.exercise06;

public class CollatzOddThread extends Thread
{
    @Override
    public void run()
    {
        while(true)
        {
            synchronized(CollatzApplication.collatzNumber)
            {
                if(CollatzApplication.collatzNumber.isOne())
                {
                    CollatzApplication.collatzNumber.notify();

                    System.out.println(Thread.currentThread().getName() + " is terminating.");

                    return;
                }

                while(CollatzApplication.collatzNumber.isOdd())
                {
                    CollatzApplication.collatzNumber.multiplyByThreeAndAddOne();
                }

                CollatzApplication.collatzNumber.notify();

                try
                {
                    System.out.println(Thread.currentThread().getName() + " is waiting.");

                    CollatzApplication.collatzNumber.wait();

                    System.out.println(Thread.currentThread().getName() + " is continuing.");
                }
                catch(InterruptedException e)
                {
                    e.printStackTrace();
                }

            }
        }
    }
}
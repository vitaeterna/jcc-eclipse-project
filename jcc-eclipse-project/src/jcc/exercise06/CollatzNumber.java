package jcc.exercise06;

public class CollatzNumber
{
    private int n = 1;

    public CollatzNumber(int n)
    {
        this.n = n;

        System.out.println("Collatz sequence starting from " + this.n);
    }

    public boolean isOne()
    {
        return n == 1;
    }

    public boolean isEven()
    {
        return n % 2 == 0;
    }

    public boolean isOdd()
    {
        return n % 2 == 1;
    }

    public void divideByTwo()
    {
        System.out.print(n + " / 2 = ");

        n = n / 2;

        System.out.println(n);
    }

    public void multiplyByThreeAndAddOne()
    {
        System.out.print("3 * " + n + " + 1 = ");

        n = 3 * n + 1;

        System.out.println(n);
    }

    @Override
    public String toString()
    {
        return String.valueOf(n);
    }
}
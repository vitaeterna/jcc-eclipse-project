package jcc.exercise09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class EchoClient
{
    public static void main(String[] args)
    {
        try
        {
            Socket socket = new Socket("127.0.0.1", 50000);

            System.out.println(socket.getLocalPort());

            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println("Test message");

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println(reader.readLine());

            socket.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
package jcc.exercise09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer
{
    public static void main(String[] args)
    {
        int port = 50000;

        try
        {
            @SuppressWarnings("resource")
            ServerSocket serverSocket = new ServerSocket(50000);

            while(true)
            {
                System.out.println("Listening on port " + port);

                Socket clientSocket = serverSocket.accept();

                System.out.println("Server: " + clientSocket.getLocalAddress() + ":" + clientSocket.getLocalPort());
                System.out.println("Client: " + clientSocket.getInetAddress() + ":" + clientSocket.getPort());

                BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String message = reader.readLine();
                System.out.println(message + "\n");

                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
                writer.println(message);
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
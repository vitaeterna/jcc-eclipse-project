package jcc.exercise08;

public class DiningPhilosophersApplication
{
    public static int numberOfPhilosophers = 5;

    public static void main(String[] args)
    {
        Object[] forks = new Object[numberOfPhilosophers];
        Philosopher[] philosophers = new Philosopher[numberOfPhilosophers];

        for(int index = 0; index < numberOfPhilosophers; index++)
        {
            forks[index] = new Object();
        }

        for(int index = 0; index < numberOfPhilosophers; index++)
        {
            Object leftFork = forks[index];
            Object rightFork = forks[(index + 1) % numberOfPhilosophers];

            if(index == numberOfPhilosophers - 1)
            {
                philosophers[index] = new Philosopher(rightFork, leftFork);
            }
            else
            {
                philosophers[index] = new Philosopher(leftFork, rightFork);
            }

            philosophers[index].setName("Philosopher " + (index + 1));
            philosophers[index].start();
        }
    }
}
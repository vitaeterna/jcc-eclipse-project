package jcc.exercise08;

import java.util.concurrent.TimeUnit;

public class Philosopher extends Thread
{
    private Object leftFork  = null;
    private Object rightFork = null;

    public Philosopher(Object leftFork, Object rightFork)
    {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    @Override
    public void run()
    {
        while(true)
        {
            long seconds = Math.round(Math.random() * 10.0d);

            System.out.println(Thread.currentThread().getName() + " is thinking for " + seconds + " seconds.");

            try
            {
                TimeUnit.SECONDS.sleep(seconds);
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }

            synchronized(leftFork)
            {
                System.out.println(Thread.currentThread().getName() + " picked up left fork.");

                synchronized(rightFork)
                {
                    System.out.println(Thread.currentThread().getName() + " picked up right fork.");

                    System.out.println(Thread.currentThread().getName() + " is eating.");

                    System.out.println(Thread.currentThread().getName() + " put down right fork.");
                }

                System.out.println(Thread.currentThread().getName() + " put down left fork.");
            }
        }
    }
}
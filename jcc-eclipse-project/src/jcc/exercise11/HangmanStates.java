package jcc.exercise11;

public class HangmanStates
{
    private static String[] states    = null;

    public static final int MIN_STATE = 0;
    public static final int MAX_STATE = 11;

    private static String   state00   = "         \n         \n         \n         \n         \n         \n";
    private static String   state01   = "         \n         \n         \n         \n _ _     \n/   \\    \n";
    private static String   state02   = "         \n  |      \n  |      \n  |      \n _|_     \n/   \\    \n";
    private static String   state03   = "  _______\n  |      \n  |      \n  |      \n _|_     \n/   \\    \n";
    private static String   state04   = "  _______\n  |/     \n  |      \n  |      \n _|_     \n/   \\    \n";
    private static String   state05   = "  _______\n  |/   | \n  |      \n  |      \n _|_     \n/   \\    \n";
    private static String   state06   = "  _______\n  |/   | \n  |    O \n  |      \n _|_     \n/   \\    \n";
    private static String   state07   = "  _______\n  |/   | \n  |    O \n  |    | \n _|_     \n/   \\    \n";
    private static String   state08   = "  _______\n  |/   | \n  |   \\O \n  |    | \n _|_     \n/   \\    \n";
    private static String   state09   = "  _______\n  |/   | \n  |   \\O/\n  |    | \n _|_     \n/   \\    \n";
    private static String   state10   = "  _______\n  |/   | \n  |   \\O/\n  |    | \n _|_  /  \n/   \\    \n";
    private static String   state11   = "  _______\n  |/   | \n  |   \\O/\n  |    | \n _|_  / \\\n/   \\    \n";

    public static String get(int state)
    {
        if(state < MIN_STATE || state > MAX_STATE)
        {
            System.err.println("HangmanStates out of bounds: " + state + " [" + MIN_STATE + ";" + MAX_STATE + "]");

            System.exit(0);
        }

        if(states == null)
        {
            states = new String[12];
            states[0] = state00;
            states[1] = state01;
            states[2] = state02;
            states[3] = state03;
            states[4] = state04;
            states[5] = state05;
            states[6] = state06;
            states[7] = state07;
            states[8] = state08;
            states[9] = state09;
            states[10] = state10;
            states[11] = state11;
        }

        return states[state];
    }
}
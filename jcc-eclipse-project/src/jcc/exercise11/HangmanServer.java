package jcc.exercise11;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HangmanServer
{
    public static void main(String[] args)
    {
        int port = 50000;

        RandomWordGenerator rwg = new RandomWordGenerator();

        System.out.println("scanned alphabet: \"" + rwg.getAlphabet() + "\"");

        try
        {
            @SuppressWarnings("resource")
            ServerSocket serverSocket = new ServerSocket(port);

            System.out.println("Listening on port " + port);

            while(true)
            {
                Socket clientSocket = serverSocket.accept();

                new HangmanServerThread(clientSocket, rwg.getWord()).start();
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
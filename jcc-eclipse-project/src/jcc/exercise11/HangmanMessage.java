package jcc.exercise11;

import java.io.Serializable;
import java.util.ArrayList;

public class HangmanMessage implements Serializable
{
    private static final long serialVersionUID = 1L;

    public int                errors           = 0;
    public String             maskedWord       = null;
    public String             guess            = null;
    public ArrayList<String>  misses           = null;
    public boolean            gameOver         = false;
    public String             solution         = null;

    public HangmanMessage(String guess)
    {
        this.guess = guess;
    }

    public HangmanMessage(int errors, String maskedWord, String guess, ArrayList<String> misses, boolean gameOver, String solution)
    {
        this.errors = errors;
        this.maskedWord = maskedWord;
        this.guess = guess;
        this.misses = misses;
        this.gameOver = gameOver;
        this.solution = solution;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(HangmanStates.get(errors));
        stringBuilder.append("Word:     ").append(addSpaces(maskedWord)).append("\n");
        stringBuilder.append("Guess:    ").append(guess == null ? "" : guess).append("\n");
        stringBuilder.append("Misses:   ").append(misses.toString());

        if(solution != null)
        {
            stringBuilder.append("\nSolution: " + solution);
        }

        return stringBuilder.toString();
    }

    public static String addSpaces(String string)
    {
        StringBuilder stringBuilder = new StringBuilder();

        for(int index = 0; index < string.length(); index++)
        {
            stringBuilder.append(string.charAt(index));

            if(index < string.length() - 1)
            {
                stringBuilder.append(" ");
            }
        }

        return stringBuilder.toString();
    }
}
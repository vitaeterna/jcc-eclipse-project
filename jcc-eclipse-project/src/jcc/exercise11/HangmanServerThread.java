package jcc.exercise11;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class HangmanServerThread extends Thread
{
    private ObjectOutputStream oos        = null;
    private ObjectInputStream  ois        = null;

    private String             secretWord = null;
    private String             maskedWord = null;

    private String             guess      = null;

    private int                errors     = 0;

    private ArrayList<String>  misses     = null;

    private boolean            gameOver   = false;

    public HangmanServerThread(Socket socket, String secretWord)
    {
        try
        {
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        this.secretWord = secretWord;

        StringBuilder string = new StringBuilder();

        for(int index = 0; index < secretWord.length(); index++)
        {
            string.append("_");
        }

        maskedWord = string.toString();

        misses = new ArrayList<>();

        execute();
    }

    private void execute()
    {
        //System.out.println("New game started: " + secretWord + "(" + socket.getRemoteSocketAddress() + ")");
        System.out.println("New game started!");

        HangmanMessage message = new HangmanMessage(errors, maskedWord, null, new ArrayList<>(misses), gameOver, null);

        while(true)
        {
            try
            {
                oos.writeUnshared(message);
                oos.flush();

                message = (HangmanMessage)ois.readUnshared();
            }
            catch(ClassNotFoundException | IOException e)
            {
                e.printStackTrace();
            }

            guess = message.guess;

            if(guess.length() == 1)
            {
                System.out.println("Guess length == 1");

                if(secretWord.contains(guess) && !maskedWord.contains(guess))
                {
                    System.out.println("Guess correct: character contained");

                    StringBuilder stringBuilder = new StringBuilder();

                    for(int index = 0; index < secretWord.length(); index++)
                    {
                        if(maskedWord.charAt(index) != '_')
                        {
                            stringBuilder.append(maskedWord.charAt(index));
                        }
                        else
                        {
                            if(secretWord.charAt(index) == guess.charAt(0))
                            {
                                stringBuilder.append(secretWord.charAt(index));
                            }
                            else
                            {
                                stringBuilder.append(maskedWord.charAt(index));
                            }
                        }
                    }

                    maskedWord = stringBuilder.toString();

                    if(!maskedWord.contains("_"))
                    {
                        gameOver = true;

                        break;
                    }
                }
                else
                {
                    errors++;

                    misses.add(guess);

                    if(errors == HangmanStates.MAX_STATE)
                    {
                        System.out.println("errors == MAX_STATE");

                        gameOver = true;

                        break;
                    }
                    else
                    {
                        System.out.println("errors < MAX_STATE");

                        gameOver = false;
                    }
                }
            }
            else
            {
                System.out.println("Guess length > 1");

                if(secretWord.equals(guess))
                {
                    System.out.println("Guess correct: word identical");

                    maskedWord = HangmanMessage.addSpaces(secretWord);

                    gameOver = true;

                    break;
                }
                else
                {
                    errors++;

                    misses.add(guess);

                    if(errors == HangmanStates.MAX_STATE)
                    {
                        System.out.println("errors == MAX_STATE");

                        gameOver = true;

                        break;
                    }
                    else
                    {
                        System.out.println("errors < MAX_STATE");

                        gameOver = false;
                    }
                }
            }

            message = new HangmanMessage(errors, maskedWord, guess, new ArrayList<>(misses), gameOver, null);
        }

        try
        {
            oos.writeObject(new HangmanMessage(errors, maskedWord, guess, new ArrayList<>(misses), gameOver, secretWord));
            oos.flush();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
package jcc.exercise11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomWordGenerator
{
    private String       file     = "./data/corncob_lowercase_reduced.txt";

    private List<String> words    = null;

    private String       alphabet = null;

    public RandomWordGenerator()
    {
        readWordsFromFile();
    }

    private void readWordsFromFile()
    {
        try(Stream<String> stream = Files.lines(Paths.get(file)))
        {
            words = stream.map(String::toLowerCase).collect(Collectors.toList());
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        System.out.println(words.size() + " words read from " + file);

        alphabet = words.stream().collect(Collectors.joining()).codePoints().distinct().sorted().collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }

    public String getWord()
    {
        return words.get(ThreadLocalRandom.current().nextInt(0, words.size() + 1));
    }

    public String getAlphabet()
    {
        return alphabet;
    }
}
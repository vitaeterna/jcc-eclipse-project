package jcc.exercise11;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class HangmanClient
{
    public static void main(String[] args)
    {
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;

        Scanner scanner = new Scanner(System.in);

        String guess = null;

        try
        {
            Socket socket = new Socket("127.0.0.1", 50000);

            try
            {
                oos = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            HangmanMessage message = null;

            while(true)
            {
                try
                {
                    message = (HangmanMessage)ois.readUnshared();

                    System.out.println(message);

                    if(message.gameOver)
                    {
                        if(message.errors == HangmanStates.MAX_STATE)
                        {
                            System.out.println("You lost!");
                        }
                        else
                        {
                            System.out.println("You won!");
                        }

                        scanner.close();

                        break;
                    }

                    do
                    {
                        System.out.print(">");

                        guess = scanner.nextLine().toLowerCase();
                    }
                    while(guess.equals("") || message.misses != null && message.misses.contains(guess) || message.maskedWord != null && message.maskedWord.contains(guess));

                    oos.writeUnshared(new HangmanMessage(guess));
                    oos.flush();
                }
                catch(ClassNotFoundException e)
                {
                    e.printStackTrace();
                }
            }

            socket.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
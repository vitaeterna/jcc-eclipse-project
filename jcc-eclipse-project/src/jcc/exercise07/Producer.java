package jcc.exercise07;

public class Producer extends Thread
{
    SynchronizedIntBuffer buffer = null;

    public Producer(SynchronizedIntBuffer buffer)
    {
        this.buffer = buffer;
    }

    @Override
    public void run()
    {
        for(int index = 1; index <= 100; index++)
        {
            buffer.produce(index);
        }
    }
}
package jcc.exercise07;

public class ProducerConsumerApplication
{
    public static void main(String[] args)
    {
        SynchronizedIntBuffer buffer = new SynchronizedIntBuffer(5);

        Producer producer = new Producer(buffer);
        producer.setName("Producer");
        Consumer consumer = new Consumer(buffer);
        consumer.setName("Consumer");

        producer.start();
        consumer.start();
    }
}
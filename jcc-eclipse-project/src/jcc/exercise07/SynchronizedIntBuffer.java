package jcc.exercise07;

import java.util.LinkedList;

public class SynchronizedIntBuffer
{
    private LinkedList<Integer> fifoBuffer = new LinkedList<>();
    private int                 maxSize    = 0;

    public SynchronizedIntBuffer(int maxSize)
    {
        this.maxSize = maxSize;
    }

    public synchronized void produce(int n)
    {
        if(fifoBuffer.size() == maxSize)
        {
            try
            {
                System.out.println(Thread.currentThread().getName() + " is waiting.");

                this.wait();

                System.out.println(Thread.currentThread().getName() + " is continuing.");
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        System.out.println("Produced: " + n);

        fifoBuffer.addLast(n);

        notify();
    }

    public synchronized int consume()
    {
        if(fifoBuffer.isEmpty())
        {
            try
            {
                System.out.println(Thread.currentThread().getName() + " is waiting.");

                this.wait();

                System.out.println(Thread.currentThread().getName() + " is continuing.");
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        System.out.println("Consumed: " + fifoBuffer.getFirst());

        notify();

        return fifoBuffer.removeFirst();
    }
}
package jcc.exercise07;

public class Consumer extends Thread
{
    SynchronizedIntBuffer buffer = null;

    public Consumer(SynchronizedIntBuffer buffer)
    {
        this.buffer = buffer;
    }

    @Override
    public void run()
    {
        for(int index = 1; index <= 100; index++)
        {
            buffer.consume();
        }
    }
}
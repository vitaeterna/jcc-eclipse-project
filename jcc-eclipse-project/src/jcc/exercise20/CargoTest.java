package jcc.exercise20;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import jcc.exercise01.Cargo;

public class CargoTest
{
    private Cargo cargo = null;

    @BeforeEach
    public void init()
    {
        cargo = new Cargo();
    }

    @ParameterizedTest
    @ValueSource(ints = { 10, 25, 1337 })
    public void setWeightTest1(int weight)
    {
        cargo.setWeight(weight);

        assertEquals(cargo.getWeight(), weight);
    }

    @ParameterizedTest
    @ValueSource(ints = { 0, -10, -25, -1337 })
    public void setWeightTest2(int weight)
    {
        cargo.setWeight(weight);

        assertEquals(cargo.getWeight(), 0);
    }
}
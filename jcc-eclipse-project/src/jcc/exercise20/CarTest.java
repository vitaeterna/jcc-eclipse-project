package jcc.exercise20;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import jcc.exercise01.Car;
import jcc.exercise01.Cargo;

public class CarTest
{
    private Car car = null;

    @BeforeEach
    public void init()
    {
        car = new Car();
    }

    @ParameterizedTest
    @ValueSource(strings = { "Mercedes", "BMW", "VW" })
    public void setBrandTest1(String brand)
    {
        car.setBrand(brand);

        assertEquals(car.getBrand(), brand);
    }

    @Test
    public void setBrandTest2()
    {
        car.setBrand(null);

        assertNull(car.getBrand());
    }

    @ParameterizedTest
    @ValueSource(strings = { "AMG", "M3", "Golf" })
    public void setTypeTest1(String type)
    {
        car.setType(type);

        assertEquals(car.getType(), type);
    }

    @Test
    public void setTypeTest2()
    {
        car.setType(null);

        assertNull(car.getType());
    }

    @ParameterizedTest
    @ValueSource(ints = { 60, 110, 150 })
    public void setHpNumberTest1(int hpNumber)
    {
        car.setHpNumber(hpNumber);

        assertEquals(car.getHpNumber(), hpNumber);
    }

    @ParameterizedTest
    @ValueSource(ints = { 0, -60, -110, -150 })
    public void setHpNumberTest2(int hpNumber)
    {
        car.setHpNumber(hpNumber);

        assertEquals(car.getHpNumber(), 0);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 4.8d, 6.7d, 12.0d })
    public void setFuelConsumptionTest1(double fuelConsumption)
    {
        car.setFuelConsumption(fuelConsumption);

        assertEquals(car.getFuelConsuption(), fuelConsumption);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 0.0d, -4.8d, -6.7d, -12.0d })
    public void setFuelConsumptionTest2(double fuelConsumption)
    {
        car.setFuelConsumption(fuelConsumption);

        assertEquals(car.getFuelConsuption(), 0.0d);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 25.0d, 32.5d, 54.8d })
    public void setFuelTankSizeTest1(double fuelTankSize)
    {
        car.setFuelTankSize(fuelTankSize);

        assertEquals(car.getFuelTankSize(), fuelTankSize);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 0.0d, -25.0d, -32.5d, -54.8d })
    public void setFuelTankSizeTest2(double fuelTankSize)
    {
        car.setFuelTankSize(fuelTankSize);

        assertEquals(car.getFuelTankSize(), 0.0d);
    }

    @ParameterizedTest
    @ValueSource(ints = { 50, 90, 130 })
    public void setMaximumCargoWeightTest1(int maximumCargoWeight)
    {
        car.setMaximumCargoWeight(maximumCargoWeight);

        assertEquals(car.getMaximumCargoWeight(), maximumCargoWeight);
    }

    @ParameterizedTest
    @ValueSource(ints = { 0, -50, -90, -130 })
    public void setMaximumCargoWeightTest2(int maximumCargoWeight)
    {
        car.setMaximumCargoWeight(maximumCargoWeight);

        assertEquals(car.getMaximumCargoWeight(), 0);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 25.0d, 32.5d, 54.8d })
    public void setFuelTest1(double fuel)
    {
        car.setFuelTankSize(60.0d);
        car.setFuel(fuel);

        assertEquals(car.getFuel(), fuel);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 0.0d, -25.0d, -32.5d, -54.8d })
    public void setFuelTest2(double fuel)
    {
        car.setFuelTankSize(60.0d);
        car.setFuel(fuel);

        assertEquals(car.getFuel(), 0.0d);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 25.0d, 32.5d, 54.8d })
    public void setFuelTest3(double fuel)
    {
        car.setFuelTankSize(25.0d);
        car.setFuel(fuel);

        assertEquals(car.getFuel(), 25.0d);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 25.0d, 32.5d, 54.8d })
    public void setRefuelTest(double fuelTankSize)
    {
        car.setFuelTankSize(fuelTankSize);
        car.refuel();

        assertEquals(car.getFuel(), fuelTankSize);
    }

    @ParameterizedTest
    @ValueSource(ints = { 50, 90, 130 })
    public void loadTest1(int weight)
    {
        Cargo cargo = mock(Cargo.class);

        when(cargo.getWeight()).thenReturn(weight);

        car.setMaximumCargoWeight(130);
        car.load(cargo);

        verify(cargo).getWeight();

        assertSame(car.getCargo(), cargo);
    }

    @ParameterizedTest
    @ValueSource(ints = { 50, 90, 130 })
    public void loadTest2(int weight)
    {
        Cargo cargo = mock(Cargo.class);

        when(cargo.getWeight()).thenReturn(weight);

        car.setMaximumCargoWeight(40);
        car.load(cargo);

        verify(cargo).getWeight();

        assertNull(car.getCargo());
    }

    @Test
    public void loadTest3()
    {
        Cargo cargo1 = mock(Cargo.class);

        when(cargo1.getWeight()).thenReturn(50);

        Cargo cargo2 = mock(Cargo.class);

        when(cargo1.getWeight()).thenReturn(90);

        car.setMaximumCargoWeight(130);
        car.load(cargo1);
        car.load(cargo2);

        verify(cargo1).getWeight();

        assertSame(car.getCargo(), cargo1);
    }

    @Test
    public void loadTest4()
    {
        car.load(null);

        assertNull(car.getCargo());
    }

    @Test
    public void loadTest5()
    {
        Cargo cargo = mock(Cargo.class);

        when(cargo.getWeight()).thenReturn(90);

        car.setMaximumCargoWeight(130);
        car.load(cargo);

        verify(cargo).getWeight();

        car.load(null);

        assertSame(car.getCargo(), cargo);
    }

    @Test
    public void unloadTest()
    {
        Cargo cargo1 = mock(Cargo.class);

        when(cargo1.getWeight()).thenReturn(50);

        Cargo cargo2 = mock(Cargo.class);

        when(cargo1.getWeight()).thenReturn(90);

        car.setMaximumCargoWeight(130);
        car.load(cargo1);

        verify(cargo1).getWeight();

        car.unload();

        assertNull(car.getCargo());

        car.load(cargo2);

        verify(cargo2).getWeight();

        assertSame(car.getCargo(), cargo2);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 0.0d, -200.0d, -1337.0d })
    public void driveTest1(double distance)
    {
        car.setFuelConsumption(4.8d);
        car.setFuelTankSize(60.0d);
        car.refuel();

        car.drive(distance);

        assertEquals(car.getFuel(), car.getFuelTankSize());
    }

    @ParameterizedTest
    @ValueSource(doubles = { 100.0d, 250.0d, 1000.0d })
    public void driveTest2(double distance)
    {
        car.setFuelConsumption(100.0d);
        car.setFuelTankSize(1001.0d);
        car.refuel();

        car.drive(distance);

        assertTrue(car.getFuel() > 0.0d);
    }

    @ParameterizedTest
    @ValueSource(doubles = { 100.0d, 250.0d, 1000.0d })
    public void driveTest3(double distance)
    {
        car.setFuelConsumption(100.0d);
        car.setFuelTankSize(50.0d);
        car.refuel();

        car.drive(distance);

        assertEquals(car.getFuel(), 0.0d);
    }

    @Test
    public void driveTest4()
    {
        car.setFuelConsumption(100.0d);
        car.setFuelTankSize(500.0d);
        car.refuel();

        car.drive(200.0d);

        assertEquals(car.getFuel(), 300.0d);
    }

    @Test
    public void driveTest5()
    {
        car.setFuelConsumption(4.8d);
        car.setFuelTankSize(60.0d);
        car.refuel();

        car.drive(100.0d);

        assertEquals(car.getFuel(), 55.2d);
    }
}
package jcc.topic06.exceptionThrowing;

public class Main
{
    public static void main(String[] args)
    {
        try
        {
            method();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void method() throws Exception
    {
        try
        {
            throw new Exception();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}
package jcc.topic06.arrayIndexOutOfBounds;

public class Main
{
    public static void main(String[] args)
    {
        int[] intArray = new int[5];

        try
        {
            intArray[7] = 0;
        }
        catch(ArrayIndexOutOfBoundsException e)
        {
            System.out.println("Index out of bounds!");
        }
    }
}
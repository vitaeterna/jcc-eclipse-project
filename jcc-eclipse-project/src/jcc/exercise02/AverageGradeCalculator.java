package jcc.exercise02;

public class AverageGradeCalculator
{
    private ExamResult[] examResults     = new ExamResult[3];
    private int          examResultCount = 0;

    public void add(ExamResult examResult)
    {
        if(examResult == null)
        {
            return;
        }

        if(examResultCount < examResults.length)
        {
            examResults[examResultCount] = examResult;

            examResultCount++;
        }
        else
        {
            System.out.println("Storage is full!");
        }
    }

    public double calculateAverageGrade()
    {
        double weightedGradesSum = 0.0d;
        double ectsSum = 0.0d;

        ExamResult currentExamResult = null;

        for(int index = 0; index < examResultCount; index++)
        {
            currentExamResult = examResults[index];

            weightedGradesSum += currentExamResult.getECTSPoints() * currentExamResult.getGrade();
            ectsSum += currentExamResult.getECTSPoints();
        }

        return weightedGradesSum / ectsSum;
    }
}
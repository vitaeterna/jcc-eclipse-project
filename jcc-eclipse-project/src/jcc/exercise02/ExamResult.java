package jcc.exercise02;

public class ExamResult
{
    private String courseName = null;
    private double ectsPoints = 0.0d;
    private double grade      = 0.0d;

    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public String getCourseName()
    {
        return courseName;
    }

    public void setECTSPoints(double ectsPoints)
    {
        if(ectsPoints < 0.0d)
        {
            this.ectsPoints = 0.0d;
        }
        else
        {
            this.ectsPoints = ectsPoints;
        }
    }

    public double getECTSPoints()
    {
        return ectsPoints;
    }

    public void setGrade(double grade)
    {
        if(grade < 0.0d)
        {
            this.grade = 0.0d;
        }
        else
        {
            this.grade = grade;
        }
    }

    public double getGrade()
    {
        return grade;
    }
}
package jcc.exercise02;

public class AverageGradeCalculatorApplication
{
    public static void main(String[] args)
    {
        ExamResult examResult1 = new ExamResult();
        examResult1.setCourseName("GdI-IFP: Introduction to Functional Programming");
        examResult1.setECTSPoints(6.0d);
        examResult1.setGrade(1.0d);

        ExamResult examResult2 = new ExamResult();
        examResult2.setCourseName("GdI-MTL: Modal and Temporal Logic");
        examResult2.setECTSPoints(6.0d);
        examResult2.setGrade(2.0d);

        ExamResult examResult3 = new ExamResult();
        examResult3.setCourseName("I3S-JCC: Java Crash Course");
        examResult3.setECTSPoints(0.0d);
        examResult3.setGrade(1.0d);

        ExamResult examResult4 = new ExamResult();
        examResult4.setCourseName("Dummy");
        examResult4.setECTSPoints(0.0d);
        examResult4.setGrade(1.0d);

        AverageGradeCalculator averageGradeCalculator = new AverageGradeCalculator();
        averageGradeCalculator.add(examResult1);
        averageGradeCalculator.add(examResult2);

        System.out.println(averageGradeCalculator.calculateAverageGrade());
    }
}